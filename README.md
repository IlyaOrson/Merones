# Merons in the Heisenberg model.

An anti-ferromagnetic quantum spin chain relates to the Heisenberg model via the Haldane-Affleck map. This implies the possibility that the Haldane conjecture reveals itself as a topological phase transition in the bidimensional O(3) model.

The Haldane Conjecture predicts a gapless 2nd order phase transition when the topological charge (theta-term in the action) has the value of pi. By using the *meron cluster algorithm* in Monte Carlo simulations we study the possible coupling of merons with the classical or restrained action. The minimal possible coupling of a given configuration is a non-polinomial problem so we use the *simmulated-annealing algorithm* to give a heuristic solution. The project is written in the *julia programming language*.

We hope to find out if a transition similar to the Berezinskiı̆-Kosterlitz-Thouless Transition in the XY model occurs since the pair neutralization in the Affleck picture is stochastic and doesn't require symetry breaking either.

See the nobel review article [Berezinskiı̆-Kosterlitz-Thouless Transition and the Haldane
Conjecture: Highlights of the Physics Nobel Prize 2016](https://arxiv.org/abs/1612.06132) and references therein.

# Results

This undergraduate project diverged several times but finally ended in a publication with a different taste than initially intended.

The code in this repo contains the basis of most of the numerical simulations in the final article:
[Topological Susceptibility of the 2d O(3) Model under Gradient Flow](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.98.114501).
[Free version](https://arxiv.org/abs/1808.08129).

Unfortunately, the code is messy, has tons of spanglish and just works on the old Julia 0.6.
This is terrible for scientific reproducibility and will not happen in future projects...

Cheers!
