## https://webhome.weizmann.ac.il/home/feamit/nodalweek/c_joas_nodalweek.pdf
## https://www.ocf.berkeley.edu/~fricke/projects/hoshenkopelman/hoshenkopelman.html
## https://gist.github.com/tobin/909424

# Determina recursivamente las relaciones de equivalencia entre dos etiquetas
# y guarda la menor etiqueta relacionada a cierto valor de etiqueta en equivalencias.
#
#   equivalencias --> vector r = [1,1,2,3,4,2,2,5,3,2,5,6,...]
#       indice i corresponde a una etiqueta con ese valor
#       el valor r[i] corresponde a la etiqueta menor (r[i] < i)
#       que es equivalente a la etiqueta i
#   menor --> valor de la etiqueta menor
#   mayor --> valor de la etiqueta mayor
function relaciones!(equivalencias::Vector{T}, menor::T, mayor::T) where T<:Integer

    if menor != mayor # Ambas etiquetas tienen el mismo valor: nada por hacer.
        relacionado = equivalencias[mayor] # la etiqueta previamente relacionada a mayor

        # Si hasta ahora esa etiqueta era independiente
        if relacionado == mayor
            equivalencias[mayor] = menor # relacionar la etiqueta mayor con la menor

        # Si hay una relación intermedia, modificar el vector de equivalencias para registrarla
        elseif menor < relacionado
            relaciones!(equivalencias, menor, relacionado)
        elseif relacionado < menor
            relaciones!(equivalencias, relacionado, menor)
        end
    end
end

# Une clases de equivalencia cuyas relaciones no se habían identificado antes.
function clases_equivalentes!(equivalencias::Vector{<:Integer})
    for (i,e) in enumerate(equivalencias)
        if i != e
            equivalencias[i] = equivalencias[e]
        end
    end
end

# Asigna el menor número de etiquetas posibles a la matriz de etiquetas
# considerando las equivalencias encontradas previamente.
function reconocimiento!(Π::AbstractMatrix{T}, equivalencias::Vector{T}) where T<:Integer
    for i in eachindex(Π)
        Π[i] = equivalencias[Π[i]]
    end
end

# Acción Estándar
# Esta función acepta el enlace con una probabilidad de Boltzmann
# Algoritmo de Metropolis, w = Wolff vector
function enlace(s1::T, s2::T, w::T, β::Real) where T<:AbstractVector{<:Real}
    ΔH = 2.0*β*(s1⋅w)*(s2⋅w)
    if ΔH < 0.0 && rand() < 1-exp(ΔH)
      return true
    else
      return false
    end
end

# Acción Restringida
# Esta función resulta en verdadero si los vectores s1 y s2
# no cumplen la restricción angular (Θ < δ).
function enlace(s1::T, s2::T, δ::Real) where T<:AbstractVector{<:Real}
    Θ = acos(s1 ⋅ s2)
    abs(Θ) > δ
end

# Acción Mixta
# Esta función resulta en verdadero si los vecores s1 y r2
# no cumplen la restricción angular (Θ < δ)
# y si se da el enlace entre s1 y s2 con una probabilidad de Boltzmann.
function enlace(s1::T, s2::T, r2::T, w::T, δ::Real, β::Real) where T<:AbstractVector{<:Real}
    Θ = acos(s1 ⋅ r2)
    if abs(Θ) > δ
        return true
    else
        ΔH = 2.0*β*(s1⋅w)*(s2⋅w)
        if ΔH < 0.0 && rand() < 1-exp(ΔH)
          return true
        else
          return false
        end
    end
end

# Etiqueta adecuadamente a los vecinos una vez determinado si están relacionados.
function inspector!(equivalencias, Π, liga_abajo, liga_derecha, i, j, imas, jmas)
    e_ij = Π[i,j]
    if liga_abajo # Relacionado con el de abajo.
        e_imas_j = Π[imas,j]
        if liga_derecha # Relacionado con el de abajo y derecha.
            e_i_jmas = Π[i,jmas]
            if iszero(e_imas_j) # El vecino de abajo no ha sido etiquetado antes.
                if iszero(e_i_jmas) # Ninguno de los vecinos ha sido etiquetado antes.
                    Π[imas,j] = e_ij
                    Π[i,jmas] = e_ij
                else # Sólo el de abajo no ha sido etiquetado.
                    Π[imas,j] = e_ij
                    mini, maxi = minmax(e_ij, e_i_jmas)
                    # Actualizar tabla de equivalencias.
                    relaciones!(equivalencias, mini, maxi)
                end
            elseif iszero(e_i_jmas) # Sólo el de la derecha no ha sido etiquetado.
                Π[i,jmas] = e_ij
                mini, maxi = minmax(e_ij, e_imas_j)
                # Actualizar tabla de equivalencias.
                relaciones!(equivalencias, mini, maxi)
            else # Ambos vecinos tienen etiquetas previas.
                tickets = [ e_ij, e_imas_j, e_i_jmas ]
                sort!(tickets) # Sortea las etiquetas de menor a mayor.
                # Actualizar tabla de equivalencias.
                relaciones!(equivalencias, tickets[1], tickets[2])
                relaciones!(equivalencias, tickets[2], tickets[3])
            end
        else # Sólo relacionado con el de abajo.
            if iszero(e_imas_j) # El de abajo no ha sido etiquetado antes.
                Π[imas,j] = e_ij
            else # El de abajo tiene etiqueta previa.
                mini, maxi = minmax(e_ij, e_imas_j)
                relaciones!(equivalencias, mini, maxi) # Actualizar tabla de equivalencias.
            end
        end
    elseif liga_derecha # Sólo relacionado con el de la derecha.
        e_i_jmas = Π[i,jmas]
        if iszero(e_i_jmas) # El de la derecha no había sido etiquetado.
            Π[i,jmas] = e_ij
        else # El de la derecha tiene etiqueta previa.
            mini,maxi = minmax(e_ij, e_i_jmas)
            # Actualizar tabla de equivalencias.
            relaciones!(equivalencias, mini, maxi)
        end
    end
end

# Acción Estándar
# Genera clusters al poner etiquetas en una matriz auxiliar
# y generar un vector de equivalencias.
function etiquetador!(  Π::AbstractMatrix{<:Integer},
                        σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                        renglones::Integer, columnas::Integer,
                        β::Real, r::T) where T<:AbstractVector{<:Real}

    # Si la etiqueta vale 0 significa que no ha sido etiquetado el lugar.
    etiqueta = 1 # Primera etiqueta
    equivalencias = eltype(Π)[]

    # Número máximo de etiquetas = número de espines.
    sizehint!(equivalencias, renglones*columnas)

    for j in 1:columnas, i in 1:renglones

        # Asigna etiquetas a posiciones no etiquetadas previamente.
        if iszero(Π[i,j])
            Π[i,j] = etiqueta
            push!(equivalencias, etiqueta) # Añade equivalencia de la nueva etiqueta.
            etiqueta += 1 # Usar una etiqueta mayor para futuras ocasiones.
        end

        i == renglones ? imas = 1 : imas = i+1  # imas = mod1(i+1, renglones)
        j == columnas  ? jmas = 1 : jmas = j+1  # jmas = mod1(j+1, columnas)

        # Dos espines estarán relacionados si, al considerar uno de ellos volteado (Σ),
        # la restricción de ángulo entre vecinos cercanos se rompe.

        liga_abajo = enlace(σ[i,j], Σ[imas,j], r, β) # Relacionado con el de abajo.
        liga_derecha = enlace(σ[i,j], Σ[i,jmas], r, β) # Relacionado con el de la derecha.

        inspector!(equivalencias, Π, liga_abajo, liga_derecha, i, j, imas, jmas)
    end
    clases_equivalentes!(equivalencias)
    reconocimiento!(Π, equivalencias)
end

# Acción Restringida
# Genera clusters al poner etiquetas en una matriz auxiliar y generar un vector de equivalencias.
function etiquetador!(Π::AbstractMatrix{<:Integer},
                      σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                      renglones::Integer, columnas::Integer,
                      δ::Real) where T<:AbstractVector{<:Real}

    etiqueta = 1 # La primera etiqueta. Si la etiqueta vale 0 significa que no ha sido etiquetado el lugar.
    equivalencias = eltype(Π)[]
    sizehint!(equivalencias, renglones*columnas) # Número máximo de etiquetas = número de espines.

    for j in 1:columnas, i in 1:renglones

        # Asigna etiquetas a posiciones no etiquetadas previamente.
        if Π[i,j] == 0
            Π[i,j] = etiqueta
            push!(equivalencias, etiqueta) # Añade equivalencia de la nueva etiqueta.
            etiqueta += 1 # Usar una etiqueta mayor para futuras ocasiones.
        end

        i == renglones ? imas = 1 : imas = i+1  # imas = mod1(i+1, renglones)
        j == columnas  ? jmas = 1 : jmas = j+1  # jmas = mod1(j+1, columnas)

        # Dos espines estarán relacionados si, al considerar uno de ellos volteado (Σ),
        # la restricción de ángulo entre vecinos cercanos se rompe.

        liga_abajo = enlace(σ[i,j], Σ[imas,j], δ) # Relacionado con el de abajo.
        liga_derecha = enlace(σ[i,j], Σ[i,jmas], δ) # Relacionado con el de la derecha.

        inspector!(equivalencias, Π, liga_abajo, liga_derecha, i, j, imas, jmas)
    end
    clases_equivalentes!(equivalencias)
    reconocimiento!(Π, equivalencias)
end

# Acción Mixta
# Genera clusters al poner etiquetas en una matriz auxiliar y generar un vector de equivalencias.
function etiquetador!(Π::AbstractMatrix{<:Integer},
                      σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                      renglones::Integer, columnas::Integer,
                      δ::Real, β::Real, r::T) where T<:AbstractVector{<:Real}
    etiqueta = 1 # La primera etiqueta. Si la etiqueta vale 0 significa que no ha sido etiquetado el lugar.
    equivalencias = eltype(Π)[]
    sizehint!(equivalencias, renglones*columnas)  # Número máximo de etiquetas = número de espines.

    for j in 1:columnas, i in 1:renglones

        # Asigna etiquetas a posiciones no etiquetadas previamente.
        if Π[i,j] == 0
            Π[i,j] = etiqueta
            push!(equivalencias, etiqueta) # Añade equivalencia de la nueva etiqueta.
            etiqueta += 1 # Usar una etiqueta mayor para futuras ocasiones.
        end

        i == renglones ? imas = 1 : imas = i+1  # imas = mod1(i+1, renglones)
        j == columnas  ? jmas = 1 : jmas = j+1  # jmas = mod1(j+1, columnas)

        # Dos espines estarán relacionados si, al considerar uno de ellos volteado (Σ),
        # la restricción de ángulo entre vecinos cercanos se rompe
        # o si por la probabilidad de Boltzmann se forma la unión.

        liga_abajo = enlace(σ[i,j], σ[imas,j], Σ[imas,j], r, δ, β) # Relacionado con el de abajo.
        liga_derecha = enlace(σ[i,j], σ[i,jmas], Σ[i,jmas], r, δ, β) # Relacionado con el de la derecha.

        inspector!(equivalencias, Π, liga_abajo, liga_derecha, i, j, imas, jmas)
    end
    clases_equivalentes!(equivalencias)
    reconocimiento!(Π, equivalencias)
end

# Crea un diccionario: etiqueta --> miembros(índices de matriz)
function mapa(Π::AbstractMatrix{I}) where I<:Integer
    diccionario = Dict{I, Vector{Tuple{Int64,Int64}}}()
    sizehint!(diccionario, length(Π))

    renglones, columnas = size(Π)
    for j in 1:columnas, i in 1:renglones
        etiqueta = Π[i,j]
        # push!(get!(diccionario, etiqueta, Tuple{Int64,Int64}[]),(i,j))
        if haskey(diccionario, etiqueta)
            push!(diccionario[etiqueta], (i,j))
        else
            diccionario[etiqueta] = [(i,j)]
        end
    end
    # Devolver sólo el arreglo de arreglos de tuplas: el arreglo de clusters.
    return values(diccionario)
end
