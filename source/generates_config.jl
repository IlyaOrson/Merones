# Genera un espín aleatorio como un vector estático.
function rand_spin()
    ϕ = 2π*rand()
    θ = acos(2rand()-1)
    SVector(sin(ϕ)*sin(θ), cos(ϕ)*sin(θ), cos(θ))
end

# Genera una configuración de espines aleatorios.
function rand_spins(N::Integer, M::Integer; container = :normal)
    if container == :mutable
        matrix =  MMatrix{N,M,SVector{3,Float64}}()
    elseif container == :normal
        matrix = Matrix{SVector{3,Float64}}(N,M)
    else
        error("container = {:mutable, :normal}")
    end
    for i in eachindex(matrix)
        matrix[i] = rand_spin()
    end
    matrix
end

# Dado un espín encuentra otro que cumpla que el ángulo entre ellos sea menor a δ.
function rand_vecinos(spin::T, δ::Real) where T<:AbstractVector{<:Real}
    while true
        propuesta = rand_spin()
        Θ = acos(propuesta ⋅ spin)
        if abs(Θ) < δ
            return propuesta
        end
    end
end

# Dados dos espines encuentra otro que cumpla que los ángulos entre el nuevo y los viejos sean menores a δ.
function rand_vecinos(spin1::T, spin2::T, δ::Real) where T<:AbstractVector{<:Real}
    inicio = time()
    while true
        propuesta = rand_spin()
        Θ1 = acos(propuesta ⋅ spin1)
        Θ2 = acos(propuesta ⋅ spin2)
        if abs(Θ1) < δ && abs(Θ2) < δ
            return propuesta
        end
        if (time()-inicio) > 10.0
            error("No se logró. ¡Inténtalo otra vez!")
        end
    end
end

# Dados tres espines encuentra otro que cumpla que los ángulos entre el nuevo y los viejos sean menores a δ.
function rand_vecinos(spin1::T, spin2::T, spin3::T, δ::Real) where T<:AbstractVector{<:Real}
    inicio = time()
    while true
        propuesta = rand_spin()
        Θ1 = acos(propuesta ⋅ spin1)
        Θ2 = acos(propuesta ⋅ spin2)
        Θ3 = acos(propuesta ⋅ spin3)
        if abs(Θ1) < δ && abs(Θ2) < δ && abs(Θ3) < δ
            return propuesta
        end
        if (time()-inicio) > 10.0
            error("No se logró. ¡Inténtalo otra vez!")
        end
    end
end

# Dados cuatro espines encuentra otro que cumpla que los ángulos entre el nuevo y los viejos sean menores a δ.
function rand_vecinos(spin1::T, spin2::T, spin3::T, spin4::T, δ::Real) where T<:AbstractVector{<:Real}
    inicio = time()
    while true
        propuesta = rand_spin()
        Θ1 = acos(propuesta ⋅ spin1)
        Θ2 = acos(propuesta ⋅ spin2)
        Θ3 = acos(propuesta ⋅ spin3)
        Θ4 = acos(propuesta ⋅ spin4)
        if abs(Θ1) < δ && abs(Θ2) < δ && abs(Θ3) < δ && abs(Θ4) < δ
            return propuesta
        end
        if (time()-inicio) > 10.0
            error("No se logró. ¡Inténtalo otra vez!")
        end
    end
end

# Genera una configuración de espines en donde todos los ángulos entre vecinos sean menores a δ.
function rand_spins_delta(renglones::Integer, columnas::Integer, δ::Real)

    if δ < 0.5π
        error("Necesito una δ mayor a π/2")
    end

    matrix = MMatrix{renglones, columnas, SVector{3,Float64}}()

    matrix[1] = rand_spin()

    for i in 2:renglones-1
        matrix[i,1] = rand_vecinos(matrix[i-1,1], δ)
    end
    for j in 2:columnas-1
        matrix[1,j] = rand_vecinos(matrix[1,j-1], δ)
    end
    for i in 2:renglones-1, j in 2:columnas-1
        matrix[i,j] = rand_vecinos(matrix[i-1,j], matrix[i,j-1], δ)
    end

    matrix[end,1] = rand_vecinos(matrix[end-1,1], matrix[1,1], δ)
    matrix[1,end] = rand_vecinos(matrix[1,end-1], matrix[1,1], δ)

    for i in 2:renglones-1
        matrix[i,end] = rand_vecinos(matrix[i-1,end], matrix[i, end-1] , matrix[i, 1], δ)
    end
    for j in 2:columnas-1
        matrix[end,j] = rand_vecinos(matrix[end,j-1], matrix[end-1,j] , matrix[1, j], δ)
    end
    matrix[end,end] = rand_vecinos(matrix[1,end],matrix[end-1,end],matrix[end,1],matrix[end,end-1],δ)

    return matrix
end

# Verifica que todos los espines de una configuración cumplan que los ángulos entre vecinos sean siempre menores al ángulo δ.
function verificentro(arreglo::AbstractMatrix{T}, δ::Real) where T<:AbstractVector{<:Real}
    renglones, columnas = size(arreglo)
    for j in 1:columnas, i in 1:renglones

        i == renglones ? i_pos = 1 : i_pos = i+1
        j == columnas  ? j_pos = 1 : j_pos = j+1

        Θ_bajo = abs(acos(arreglo[i_pos, j] ⋅ arreglo[i,j]))
        Θ_derecha = abs(acos(arreglo[i, j_pos] ⋅ arreglo[i,j]))

        Θ_bajo < δ || error("No se cumple la condición entre ",(i,j), " y ",(i_pos,j))
        Θ_derecha < δ || error("No se cumple la condición entre ", (i,j), " y ",(i,j_pos))
    end
    println("Se cumple la condición.")
end

# Encuentra los vecinos más cercanos.
function vecinos(coord::Tuple{T,T}, renglones::T, columnas::T) where T<:Integer
    i, j = coord

    i == renglones ? i_pos = 1 : i_pos = i+1  # i_pos = mod1(i+1, renglones)
    j == columnas  ? j_pos = 1 : j_pos = j+1  # j_pos = mod1(j+1, columnas)

    i == 1 ? i_neg = renglones : i_neg = i-1  # i_neg = mod1(i-1, renglones)
    j == 1 ? j_neg = columnas  : j_neg = j-1  # j_neg = mod1(j-1, columnas)

    return [(i,j_pos),(i,j_neg), (i_pos,j), (i_neg,j)]
end
