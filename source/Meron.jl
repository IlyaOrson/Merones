
# __precompile__()

module Meron

using StaticArrays
using FileIO, JLD2, Glob
using LsqFit # Precompilation fails for LsqFit
using PyPlot, Colors

include("generates_config.jl")
include("hoshen_kopelman_labeling.jl")

include("single_cluster.jl")
include("multi_cluster.jl")

include("correlation.jl")

include("topological_charge.jl")
include("charge_size.jl")

include("pairing_distance.jl")
include("minimal_pairing_brute.jl")
include("minimal_pairing_annealing.jl")

include("gradient_flow.jl")
include("slab.jl")

include("graphics.jl")
include("auxiliares.jl")

include("simuladores.jl")

include("jld.jl")

end #module
