# Aplica el slab method para determinar el cuadrado de la densidad de carga en cada subvolumen por columnas.
function slab(matriz::AbstractMatrix{T}, Q2) where T<:AbstractVector{<:Real}

    renglones, columnas = size(matriz)
    q2_prima_parciales = zeros(columnas+1)

    for i in 1:columnas-1

        x = i/columnas

        q2 = densidad_carga_cuadrada(matriz, i)
        q2_prima = q2 - Q2 * x^2

        q2_prima_parciales[i+1] = q2_prima
    end
    return q2_prima_parciales
end

parabola(x, V, χ) = V * χ .* x.*(1.-x)

# Realiza un ajuste por mínimos cuadrados a una parábola.
function ajustador_parabola(promedios, volumen, confianza)

    tamaño = length(promedios)
    dominio = collect(0:tamaño-1)./tamaño
    modelo(x, χ) = parabola(x, volumen, χ)
    ajuste = curve_fit(modelo, dominio, promedios, [0.01])

    χ_ajustada = ajuste.param
    χ_error = estimate_errors(ajuste, confianza)

    return χ_ajustada[1], χ_error[1] # Para evitar obtener un arreglo de una sola dimensión
end

parabola_corrida(x, V, χ) = (V * χ[1] .* x.*(1-x)) + χ[2] # χ[1] = χ, χ[2] = C

# Realiza un ajuste por mínimos cuadrados a una parábola.
function ajustador_parabola_corrida(promedios, volumen, subrango, confianza)

    tamaño = length(promedios)
    dominio = collect(0:tamaño-1)./tamaño
    modelo(x, χ) = parabola_corrida(x, volumen, χ)
    ajuste = curve_fit(modelo, dominio[subrango], promedios[subrango], [0.01, 0.01])

    χ_ajustada = ajuste.param
    χ_error = estimate_errors(ajuste, confianza)

    return χ_ajustada, χ_error # Para evitar obtener un arreglo de una sola dimensión
end

# Ejecuta el algoritmo multicluster estándar β, y cada "intersweeps"
# lleva a cabo una corrida Slab. Registra un total de "cuantos_Slabs" de estas
# corridas, y en cada una de estas registra ⟨q'²⟩ en un diccionario con la Q total.
function slabber(σ::AbstractMatrix{T}, Q_max::Integer,
                 intersweeps::Integer,
                 cuantos_Slabs::Integer, β) where T<:AbstractVector{<:Real}
    Σ = copy(σ)
    termalizacion = 10^6
    termalizador_standard!(σ, Σ, β, termalizacion) # Termalización

    registro_slabs = Dict{ Float64, Vector{Vector{Float64}} }()

    for i in 1:cuantos_Slabs

        termalizador_standard!(σ, Σ, β, intersweeps)

        Q = round(abs(triangulador(σ)))
        Q <= Q_max ? Q2 = Q^2 : continue
        q2_prima_parciales = slab(σ, Q2)
        q4_prima_parciales = q2_prima_parciales.^2

        if haskey(registro_slabs, Q)
            registro = registro_slabs[Q]
            registro[1] += q2_prima_parciales
            registro[2] += q4_prima_parciales
        else
            registro_slabs[Q] = [q2_prima_parciales, q4_prima_parciales]
        end
    end
    for k in keys(registro_slabs)
        registro_slabs[k] = medias_errores(registro_slabs[k], cuantos_Slabs)
    end
    return registro_slabs
end

# Ejecuta el algoritmo multicluster estándar β, y cada "intersweeps"
# lleva a cabo una corrida Gradient Flow, realizando un total de "cuantos_GF".
# En cada una de estas ejecuta una corrida Slab en los pasos temporales que
# corresponden a [t0, 2t0, ..., flow_steps*t0]. Añade el registro de ⟨q'²⟩ en un
# diccionario que contiene todas las medidas (Q,t) => matriz con ⟨q'²⟩ en sus columnas.
function slabberAfterFlow(σ::AbstractMatrix{T}, Q_max::Integer,
                          intersweeps::Integer,
                          cuantos_GF::Integer, to,
                          flow_steps::Integer, h, β) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    volumen = length(σ)
    Σ = copy(σ)
    termalizacion = 10^6
    termalizador_standard!(σ, Σ, β, termalizacion) # Termalización

    registro_slabs = Dict{Tuple{Float64,Float64}, Vector}()
    mediciones_medias = cuantos_GF # * (columnas + renglones) <- all-to-all
    registro_correlaciones_medias = Dict(n*to => zeros(2,columnas) for n in 0:flow_steps)
    medias_espines = zeros(eltype(σ),columnas)

    # Una columna para cada flowtime y un renglon para cada medición
    registro_cargas = zeros(flow_steps+1, 2) # Registro para calcular χ = ⟨Q^2⟩/V

    congelado = copy(σ)
    for i in 1:cuantos_GF

        termalizador_standard!(σ, Σ, β, intersweeps)

        # Lo siguiente sólo funciona para matrices cuadradas...

        # Método alternativo que utiliza la media del spin sobre columnas
        mean_corr_single!(σ, medias_espines, registro_correlaciones_medias[0.0])

        registra_carga!(registro_cargas, σ, 0)

        if Q <= Q_max
            q2_prima_parciales = slab(σ, Q2)
            q4_prima_parciales = q2_prima_parciales.^2

            if haskey(registro_slabs, (Q, 0.0))
                registro = registro_slabs[Q,0.0]
                registro[1] += q2_prima_parciales
                registro[2] += q4_prima_parciales
                registro[3] += 1
            else
                registro_slabs[Q,0.0] = [q2_prima_parciales, q4_prima_parciales, 1]
            end
        end

        congelado .= σ
        for n in 1:flow_steps
            nt_ = (n-1)*to
            nt = n*to

            gradient_flow!(congelado, β, nt_, nt, h)

            # Método alternativo que utiliza la media del spin sobre columnas
            mean_corr_single!(congelado, medias_espines, registro_correlaciones_medias[nt])

            registra_carga!(registro_cargas, congelado, n)

            if Q <= Q_max
                q2_prima_parciales = slab(congelado, Q2)
                q4_prima_parciales = q2_prima_parciales.^2

                if haskey(registro_slabs, (Q,nt))
                    registro = registro_slabs[Q,nt]
                    registro[1] += q2_prima_parciales
                    registro[2] += q4_prima_parciales
                    registro[3] += 1
                else
                    registro_slabs[Q,nt] = [q2_prima_parciales, q4_prima_parciales, 1]
                end
            end
        end
    end

    # Calcular medias y errores a partir de la matriz de sumas para cada flow step
    for p in 0:flow_steps
        k = p*to
        reg_corr_medias = registro_correlaciones_medias[k]
        medias_errores_renglones!(reg_corr_medias, mediciones_medias)
    end
    # Calcular medias y errores a partir de la matriz de sumas
    medias_errores_columnas!(registro_cargas, cuantos_GF, volumen) # χ y χ_err

    # Convierte registro de sumas de slabs a registro de medias y errores.
    for k in keys(registro_slabs)
        registro_slabs[k] = medias_errores(registro_slabs[k])
    end

    return registro_slabs, registro_correlaciones_medias, registro_cargas
end
