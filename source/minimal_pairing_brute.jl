# Se utiliza para determinar las distancias correspondientes al apareamiento mínimo entre la misma especie.
function medidor_individual(apareamiento::Vector{<:Integer}, matriz::AbstractMatrix{<:Real})
    distancias = zeros(length(apareamiento)÷2)
    for i in 2:2:length(apareamiento)
        distancias[i÷2] = matriz[apareamiento[i-1], apareamiento[i]]
    end
    distancias
end

# Se utiliza para determinar la distancia total correspondientes al apareamiento mínimo entre la misma especie.
function medidor_total(apareamiento::Vector{<:Integer}, matriz::AbstractMatrix{<:Real})
    suma = 0.0
    for i in 2:2:length(apareamiento)
        suma += matriz[apareamiento[i-1],apareamiento[i]]
    end
    return suma
end

# Función recursiva que visita todas los apareamientos posibles y se queda con el mínimo.
function busca_apareamiento_minimo_ma!(la_buena::Vector{T}, la_prueba::Vector{T},
                                       matriz::AbstractMatrix{T}, disponibles::Vector{T},
                                       renglones::Integer, columnas::Integer,
                                       nivel::Integer = 1) where T <: Real
    if nivel < renglones
        for c in 1:columnas
            if disponibles[c] == 0.0; continue; end
            la_prueba[nivel] = matriz[nivel, c]
            disponibles_copia = copy(disponibles)
            disponibles_copia[c] = 0.0
            busca_apareamiento_minimo_ma!(la_buena, la_prueba, matriz, disponibles_copia,
                                        renglones, columnas, nivel+1)
        end
    else
        for c in 1:columnas
            if disponibles[c] == 0.0; continue; end
            la_prueba[nivel] = matriz[nivel, c]
            if sum(la_prueba) < sum(la_buena)
                for i in 1:renglones
                    la_buena[i] = la_prueba[i]
                end
            end
        end
    end
end

#Versión de lo anterior para elementos con el mismo signo: parejas disjuntas de un mismo grupo.
function busca_apareamiento_minimo_mm!( apareo_minimo::Vector{T},
                                        apareo::Vector{T},
                                        matriz::AbstractMatrix{<:Real},
                                        disponibles::Vector{T},
                                        faltantes::T) where T <: Integer
    if faltantes < 2
        temporal = medidor_total(apareo_minimo, matriz)
        propuesta = medidor_total(apareo, matriz)
        if propuesta < temporal
            for i in eachindex(apareo_minimo)
                apareo_minimo[i] = apareo[i]
            end
        end

    elseif faltantes % 2 != 0

        for k in 1:faltantes
            disp = copy(disponibles)
            deleteat!(disp, k)
            busca_apareamiento_minimo_mm!(apareo_minimo, apareo, matriz, disp, faltantes-1)
        end
    else
        for k in 2:faltantes
            i = disponibles[1]
            j = disponibles[k]

            disp = copy(disponibles)
            apa = copy(apareo)

            push!(apa, i, j)
            deleteat!(disp, k)
            deleteat!(disp, 1)
            busca_apareamiento_minimo_mm!(apareo_minimo, apa, matriz, disp, faltantes-2)
        end
    end
end

# Prepara la matriz de distancias y regresa la combinación con mínima distancia.
# clusters = (Índices tuplas)
function apareamiento_minima_distancia_absoluta_ma( clusters,
                                                    positivos::Vector{T},
                                                    negativos::Vector{T},
                                                    renglones_σ::T,
                                                    columnas_σ::T) where T <: Integer
    num_pos = length(positivos)
    num_neg = length(negativos)

    if num_pos <= num_neg
        matriz_distancias = zeros(num_pos, num_neg)
        for (i,p) in enumerate(positivos), (j,n) in enumerate(negativos)
            matriz_distancias[i,j] = distancia_menor(clusters[p], clusters[n],
                                                     renglones_σ, columnas_σ)
        end
        renglones = num_pos
        columnas = num_neg
    else
        matriz_distancias = zeros(num_neg, num_pos)
        for (i,n) in enumerate(negativos), (j,p) in enumerate(positivos)
            matriz_distancias[i,j] = distancia_menor(clusters[n],clusters[p],
                                                     renglones_σ, columnas_σ)
        end
        renglones = num_neg
        columnas = num_pos
    end

    propuesta = Float64[matriz_distancias[i,i] for i in 1:renglones]

    busca_apareamiento_minimo_ma!(propuesta, copy(propuesta), matriz_distancias,
                                collect(1.0:columnas), renglones, columnas)
    return propuesta
end

# Lo mismo que la anterior pero para partículas con la misma carga.
# (cargados = positivos o negativos)
# clusters = (Índices tuplas)
function apareamiento_minima_distancia_absoluta_mm( clusters,
                                                    cargados::Vector{T},
                                                    renglones::T,
                                                    columnas::T) where T <: Integer
    largo = length(cargados)
    matriz_distancias = zeros(largo, largo)
    for i in 1:largo, j in i+1:largo
        matriz_distancias[i,j] = distancia_menor(clusters[cargados[i]],
                                                 clusters[cargados[j]],
                                                 renglones, columnas)
    end

    # renglones, columnas -> largo (es una matriz triangular superior con diagonal nula)

    if largo % 2 == 0
        apareo_minimo = collect(1:largo)
    else
        apareo_minimo = collect(1:largo-1)
    end

    apareo = Int64[]
    indices = collect(1:largo)
    falt = length(indices)

    busca_apareamiento_minimo_mm!(apareo_minimo, apareo, matriz_distancias, indices, largo)
    medidor_individual(apareo_minimo, matriz_distancias)
end

# Anexa al diccionario las distancias correspondientes al
# apareamiento encontrado, para cada carga. (Índices tuplas)
function distancias_minimas_absolutas_ma!(  dict_cargas_distancias::Dict{<:Real, Vector{<:Real}},
                                            clusters, cargas::Vector{<:Real},
                                            Qmax::Real, renglones::T, columnas::T) where T <: Integer
    #Se debe asegurar que esto no pasa afuera de la función.
    if Qmax == 0.0; return println("Configuración sin parejas."); end

    for (k,Q) in enumerate(0.5:0.5:Qmax)

        positivos, negativos = clusters_opuestos(Q, cargas)
        if length(positivos) == 0 || length(negativos) == 0
            println("No hay parejas con carga Q = $Q")
            continue
        end

        distancias_cuadradas = apareamiento_minima_distancia_absoluta_ma(
                                    clusters, positivos, negativos, renglones, columnas)
        registro = get!(dict_cargas_distancias, Q, Float64[])
        dict_cargas_distancias[Q] = append!(registro, sqrt(distancias_cuadradas))
    end
end

# Anexa al diccionario las distancias correspondientes al apareamiento entre elementos del mismo conjunto,
# para cada magnitud de carga y para cada signo respectivamente. Dict: Q => (positivos, negativos)
function distancias_minimas_absolutas_mm!(  dict_cargas_distancias::Dict{<:Real, Tuple{Vector{<:Real},Vector{<:Real}}},
                                            clusters, cargas::Vector{<:Real},
                                            Qmax::Real, renglones::T, columnas::T) where T<:Integer
    #Se debe asegurar que esto no pasa afuera de la función.
    if Qmax == 0.0; return println("Configuración sin parejas."); end

    for (k,Q) in enumerate(0.5:0.5:Qmax)

        positivos, negativos = clusters_opuestos(Q, cargas)

        p = length(positivos)
        n = length(negativos)

        if p < 2
          if n < 2
            println("No hay suficientes elementos con carga Q = $Q")
            continue
          end
          distancias_cuadradas_negativos = apareamiento_minima_distancia_absoluta_mm(
                                                clusters, negativos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[2], sqrt(distancias_cuadradas_negativos))
        elseif n < 2
          distancias_cuadradas_positivos = apareamiento_minima_distancia_absoluta_mm(
                                                clusters, positivos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[1], sqrt(distancias_cuadradas_positivos))
        else
          distancias_cuadradas_positivos = apareamiento_minima_distancia_absoluta_mm(
                                                clusters, positivos, renglones, columnas)
          distancias_cuadradas_negativos = apareamiento_minima_distancia_absoluta_mm(
                                                clusters, negativos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[1], sqrt(distancias_cuadradas_positivos))
          append!(registro[2], sqrt(distancias_cuadradas_negativos))
        end
    end
end

# Realiza una simulación multicluster_constrained y
# registra las distancias entre antipartículas.
function distancia_absoluta_ma!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Vector{Float64}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_absolutas_ma!(diccionario, clusters, charges,
                                         Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, Tuple{Float64,Float64,Float64}}()

    for k in keys(diccionario)
        veces = length(diccionario[k])
        resultados[k] = (veces, mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
    end
    resultados
end

# Realiza una simulación multicluster_constrained y
# registra las distancias entre antipartículas.
function distancia_absoluta_mm!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end
        distancias_minimas_absolutas_mm!(diccionario, clusters, charges, Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{6, Float64}}()

    for k in keys(diccionario)
        pos = length(diccionario[k][1])
        neg = length(diccionario[k][2])
        resultados[k] = (pos, mean(diccionario[k][1]), std(diccionario[k][1])/sqrt(pos),
                         neg, mean(diccionario[k][2]), std(diccionario[k][2])/sqrt(neg))
    end
    resultados
end

# Realiza una simulación multicluster_semiconstrained y
# registra las distancias entre antipartículas.
function distancia_absoluta_ma!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Vector{Float64}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_absolutas_ma!(diccionario, clusters,  charges,
                                         Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, Tuple{Float64,Float64,Float64}}()

    for k in keys(diccionario)
        veces = length(diccionario[k])
        resultados[k] = (veces, mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
    end
    resultados
end

# Realiza una simulación multicluster_semiconstrained y registra las distancias entre antipartículas.
function distancia_absoluta_mm!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end
        distancias_minimas_absolutas_mm!(diccionario, clusters, charges, Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{6, Float64}}()

    for k in keys(diccionario)
        pos = length(diccionario[k][1])
        neg = length(diccionario[k][2])
        resultados[k] = (pos, mean(diccionario[k][1]), std(diccionario[k][1])/sqrt(pos),
                         neg, mean(diccionario[k][2]), std(diccionario[k][2])/sqrt(neg))
    end
    resultados
end
