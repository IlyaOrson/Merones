function termalizador_standard!(σ, Σ, β, repeticiones)
    for t in 1:repeticiones
        w = rand_spin()
        clusters = multicluster_standard!(σ, Σ, w, β)
        wang!(σ, Σ, clusters)
    end
end

function termalizador_constrained!(σ, Σ, δ, repeticiones)
    for t in 1:repeticiones
      w = rand_spin()
      clusters = multicluster_constrained!(σ, Σ, w, δ)
      wang!(σ, Σ, clusters)
    end
end

function termalizador_semiconstrained!(σ, Σ, δ, β, repeticiones)
    for t in 1:repeticiones
      w = rand_spin()
      clusters = multicluster_semiconstrained!(σ, Σ, w, δ, β)
      wang!(σ, Σ, clusters)
    end
end

function simulacion_constrained!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario_cargas_tamaño = Dict{Float64, Vector{Int64}}()
    diccionario_ma = Dict{Float64, Vector{Float64}}()
    diccionario_mm = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        tamaño_islas!(diccionario_cargas_tamaño, clusters, charges)
        distancias_minimas_annealing_ma!(diccionario_ma, clusters, charges,
                                         Qmax, renglones, columnas)
        distancias_minimas_annealing_mm!(diccionario_mm, clusters, charges,
                                         Qmax, renglones, columnas)

        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados_tamaños = Dict{Float64, Tuple{Float64,Float64,Float64}}()
    resultados_apareos_ma = Dict{Float64, Tuple{Int64,Float64,Float64}}()
    resultados_apareos_mm = Dict{   Float64, Tuple{Int64,Float64,Float64,
                                    Int64,Float64,Float64 }}()

    for k in keys(diccionario_cargas_tamaño)
        tamaños = diccionario_cargas_tamaño[k]
        veces = length(tamaños)
        resultados_tamaños[k] = (veces/clusters_totales, mean(tamaños),
                                std(tamaños)/sqrt(veces))
    end
    for k in keys(diccionario_ma)
        distancias = diccionario_ma[k]
        parejas = length(distancias)
        resultados_apareos_ma[k] = (parejas, mean(distancias), std(distancias/sqrt(parejas)))
    end
    for k in keys(diccionario_mm)
        dist_pos = diccionario_mm[k][1]
        dist_neg = diccionario_mm[k][2]
        num_pos = length(dist_pos)
        num_neg = length(dist_neg)
        resultados_apareos_mm[k] = (num_pos, mean(dist_pos), std(dist_pos)/sqrt(num_pos),
                                    num_neg, mean(dist_neg), std(dist_neg)/sqrt(num_neg))
    end
    resultados_tamaños, resultados_apareos_ma, resultados_apareos_mm
end

function simulacion_semiconstrained!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                    ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario_cargas_tamaño = Dict{Float64, Vector{Int64}}()
    diccionario_ma = Dict{Float64, Vector{Float64}}()
    diccionario_mm = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        tamaño_islas!(diccionario_cargas_tamaño, clusters, charges)
        distancias_minimas_annealing_ma!(diccionario_ma, clusters, charges,
                                            Qmax, renglones, columnas)
        distancias_minimas_annealing_mm!(diccionario_mm, clusters, charges,
                                            Qmax, renglones, columnas)

        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados_tamaños = Dict{Float64, Tuple{Float64,Float64,Float64}}()
    resultados_apareos_ma = Dict{Float64, Tuple{Int64,Float64,Float64}}()
    resultados_apareos_mm = Dict{Float64, Tuple{Int64,Float64,Float64,
                                                Int64,Float64,Float64 }}()

    for k in keys(diccionario_cargas_tamaño)
        tamaños = diccionario_cargas_tamaño[k]
        veces = length(tamaños)
        resultados_tamaños[k] = (veces/clusters_totales, mean(tamaños),
                                std(tamaños)/sqrt(veces))
    end
    for k in keys(diccionario_ma)
        distancias = diccionario_ma[k]
        parejas = length(distancias)
        resultados_apareos_ma[k] = (parejas, mean(distancias),
                                    std(distancias/sqrt(parejas)))
    end
    for k in keys(diccionario_mm)
        dist_pos = diccionario_mm[k][1]
        dist_neg = diccionario_mm[k][2]
        num_pos = length(dist_pos)
        num_neg = length(dist_neg)
        resultados_apareos_mm[k] = (num_pos, mean(dist_pos), std(dist_pos)/sqrt(num_pos),
                                    num_neg, mean(dist_neg), std(dist_neg)/sqrt(num_neg))
    end
    resultados_tamaños, resultados_apareos_ma, resultados_apareos_mm
end
