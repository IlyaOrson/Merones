# Ángulo sólido entre vecinos.
function angulo_solido(a::T, b::T, c::T) where T<:AbstractVector{<:Real}
    num = a ⋅ (b × c)
    den = 1 + b⋅c + c⋅a + a⋅b
    2*atan2(num,den) #atan2 es muy importante, atan da el resultado incorrecto.
end

# Densidead de carga sobre una plaqueta. Se asocia al espín de la esquina superior izquierda.
function plaqueta(  matriz::AbstractMatrix{T}, i, j,
                    rev = false) where T<:AbstractVector{<:Real}

    renglones, columnas = size(matriz)
    ρ_carga = 0.0

    # Para cambiar la manera en la que se forman los triángulos.
    rev ? residuo = 0 : residuo = 1

    i == renglones ? i_mas = 1 : i_mas = i+1
    j == columnas  ? j_mas = 1 : j_mas = j+1

    s_i_j = matriz[i,j]
    s_imas_j = matriz[i_mas,j]
    s_i_jmas = matriz[i,j_mas]
    s_imas_jmas = matriz[i_mas,j_mas]

    if (i+j)%2 == residuo
        ρ_carga += angulo_solido(s_i_j, s_imas_j, s_i_jmas)
        ρ_carga += angulo_solido(s_i_jmas, s_imas_j, s_imas_jmas)
    else
        ρ_carga += angulo_solido(s_i_j, s_imas_j, s_imas_jmas)
        ρ_carga += angulo_solido(s_i_j, s_imas_jmas, s_i_jmas)
    end

    return ρ_carga # A esto falta dividirlo entre 4π; se hace afuera.
end

# Desidad cuadrada media desde la primera hasta cierta columna "col".
# col = Hasta que columna estamos calculando
function densidad_carga_cuadrada(matriz::AbstractMatrix{T}, col
                                ) where T<:AbstractVector{<:Real}
    suma = 0.0
    # proba = 0.0 ### PRUEBA CON HECTOR
    for j in 1:col, i in 1:size(matriz,1) #renglones
        q = plaqueta(matriz, i, j)
        # proba += q ### PRUEBA CON HECTOR
        suma +=  q #^2 # El término 1/(4π)² se agrega después
    end
    # @show proba/4π  ### PRUEBA CON HECTOR
    return suma^2 / (4π)^2 # Media del cuadrado de la densidad
end

# Carga total de arreglo.
function triangulador(matriz::AbstractMatrix{T}, rev = false) where T<:AbstractVector{<:Real}

    renglones, columnas = size(matriz)
    carga = 0.0

    # Para cambiar la manera en la que se forman los triángulos.
    rev ? residuo = 0 : residuo = 1

    @inbounds for j in 1:columnas, i in 1:renglones

        i == renglones ? i_mas = 1 : i_mas = i+1  # i_mas = mod1(i+1, renglones)
        j == columnas  ? j_mas = 1 : j_mas = j+1  # j_mas = mod1(j+1, columnas)

        s_i_j = matriz[i,j]
        s_imas_j = matriz[i_mas,j]
        s_i_jmas = matriz[i,j_mas]
        s_imas_jmas = matriz[i_mas,j_mas]

        if (i+j)%2 == residuo
            carga += angulo_solido(s_i_j,s_imas_j,s_i_jmas)
            carga += angulo_solido(s_i_jmas,s_imas_j,s_imas_jmas)
        else
            carga += angulo_solido(s_i_j,s_imas_j,s_imas_jmas)
            carga += angulo_solido(s_i_j,s_imas_jmas,s_i_jmas)
        end
    end
    carga/4π
end

# Acción estándar.
function accion(arreglo::AbstractMatrix{T}, β::Real) where T<:AbstractVector{<:Real}

    renglones , columnas = size(arreglo)
    suma = 0.0

    for j in 1:columnas, i in 1:renglones

        i == renglones ? imas = 1 : imas = i+1  # imas = mod1(i+1, renglones)
        j == columnas  ? jmas = 1 : jmas = j+1  # jmas = mod1(j+1, columnas)

        suma += 1 - arreglo[i,j] ⋅ arreglo[imas,j]
        suma += 1 - arreglo[i,j] ⋅ arreglo[i,jmas]
    end
    β*suma
end
# Densidad de acción estándar.
function densidad_accion(arreglo::AbstractMatrix{T}, β::Real) where T<:AbstractVector{<:Real}

    renglones, columnas = size(arreglo)
    suma = 0.0

    for j in 1:columnas, i in 1:renglones

        i == renglones ? imas = 1 : imas = i+1  # imas = mod1(i+1, renglones)
        j == columnas  ? jmas = 1 : jmas = j+1  # jmas = mod1(j+1, columnas)

        suma += 1 - arreglo[i,j] ⋅ arreglo[imas,j]
        suma += 1 - arreglo[i,j] ⋅ arreglo[i,jmas]
    end
    β * suma / (renglones*columnas)
end

# Asigna las cargas correspondientes a la lista de clusters y
# la carga de la configuración original. (modifica μ)
function cargas!(σ::AbstractMatrix{T}, Σ::AbstractMatrix{T}, μ::AbstractMatrix{T}, clusters
                 ) where T<:AbstractVector{<:Real}

    carga_original = round(2.0*triangulador(σ))/2.0
    agrupaciones = length(clusters)
    lista_cargas = zeros(agrupaciones)

    for i in eachindex(σ)
        μ[i] = σ[i]
    end

    for (i,cluster) in enumerate(clusters)
        for coord in cluster #Arreglo de tuplas
            μ[coord...] = Σ[coord...]
        end
        carga_volteada = triangulador(μ)
        lista_cargas[i] = round(carga_original - carga_volteada)/2
        if lista_cargas[i] == -0.0
            lista_cargas[i] = 0.0
        end
        for coord in cluster #Arreglo de tuplas
            μ[coord...] = σ[coord...]
        end
    end
    lista_cargas, carga_original
end
