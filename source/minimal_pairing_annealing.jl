# Esta función ayuda a determinar si una columna propuesta ya está utilizada por otro miembro del apareamiento.
function indice_asociado(valor::Integer, arreglo::Vector{<:Integer})
    for (i,v) in enumerate(arreglo)
        if valor == v
            return i
        end
    end
    return 0 #Cuando no está
end

indice_pareja(indice::Integer) = indice % 2 == 0 ?  indice - 1 : indice + 1

# Muestra las distancias que representa el vector de apareamiento.
function traductor(apareamiento::Vector{<:Integer}, matriz::AbstractMatrix{<:Real})
    distancias = zeros(length(apareamiento))
    for (i,j) in enumerate(apareamiento)
        distancias[i] = matriz[i,j]
    end
    distancias
end

# Suma las distancias que representa el vector de apareamiento.
function sumador(apareamiento::Vector{<:Integer}, matriz::AbstractMatrix{<:Real})
    suma = 0.0
    for (i,j) in enumerate(apareamiento)
        suma += matriz[i,j]
    end
    suma
end

# Ejecuta el algoritmo de "simulatead annealing" (SA) encontrar el apareamiento mínimo entre meroes y antimerones.
function annealing_ma!(apareamiento::Vector{<:Integer}, matriz::AbstractMatrix{<:Real},
                       temperaturas::Vector{<:Real}, pasos_por_temperatura::Integer)

    renglones = 1:size(matriz,1)
    columnas = 1:size(matriz,2)
    suma_distancias = sumador(apareamiento, matriz)

    for T in temperaturas
        n = 0
        while n < pasos_por_temperatura

            propuesta_r = rand(renglones)
            viejo_c = apareamiento[propuesta_r]

            nuevo_c = rand(columnas)

            if nuevo_c == viejo_c; continue; end

            estorbo_r = indice_asociado(nuevo_c, apareamiento)

            if estorbo_r == 0 #No hay estorbo

                Δ = matriz[propuesta_r, nuevo_c] - matriz[propuesta_r, viejo_c]

                if Δ <= 0.0
                    apareamiento[propuesta_r] = nuevo_c
                    suma_distancias += Δ

                elseif rand() < exp(-Δ/T)
                    apareamiento[propuesta_r] = nuevo_c
                    suma_distancias += Δ
                end

            else
                Δ = matriz[propuesta_r, nuevo_c] - matriz[propuesta_r, viejo_c] +
                    matriz[estorbo_r, viejo_c] - matriz[estorbo_r, nuevo_c]

                if Δ <= 0.0
                    apareamiento[estorbo_r] = viejo_c
                    apareamiento[propuesta_r] = nuevo_c

                    suma_distancias += Δ

                elseif rand() < exp(-Δ/T)

                    apareamiento[estorbo_r] = viejo_c
                    apareamiento[propuesta_r] = nuevo_c

                    suma_distancias += Δ
                end
            end
            n += 1
        end
    end
end

# Lo mismo que la función anterior pero para elementos del mismo conjunto.
function annealing_mm!(apareo::Vector{<:Integer}, matriz::AbstractMatrix{<:Real},
                       temperaturas::Vector{<:Real}, pasos_por_temperatura::Integer)

    lugares = eachindex(apareo)
    indices_disponibles = 1:size(matriz,1) # Es una matriz cuadrada
    suma_distancias = medidor_total(apareo, matriz)

    for T in temperaturas
        n = 0
        while n < pasos_por_temperatura

            # Índices del elegido y de su pareja.
            i_elegido = rand(lugares)
            i_pareja_e = indice_pareja(i_elegido)

            # Valores asociados a los índices.
            elegido = apareo[i_elegido]
            pareja_e = apareo[i_pareja_e]

            # Valor que se propone insertar en el índice elegido.
            cambio = rand(indices_disponibles) # Por construcción: i_cambio = cambio # OJO
            # No puedes proponer al mismo o a la pareja
            while cambio == elegido || cambio == pareja_e
                cambio = rand(indices_disponibles)
            end

            i_ocupado = indice_asociado(cambio, apareo)

            if i_ocupado == 0 # El cambio elegido está disponible.

                Δ = matriz[cambio, pareja_e] - matriz[elegido, pareja_e]

                if Δ <= 0.0
                    apareo[i_elegido] = cambio
                    suma_distancias += Δ

                elseif rand() < exp(-Δ/T)
                    apareo[i_elegido] = cambio
                    suma_distancias += Δ
                end

            else # El cambio elegido ya se encuentra apareado.

                i_pareja_o = indice_pareja(i_ocupado)
                pareja_o = apareo[i_pareja_o]

                Δ = matriz[cambio, pareja_e] - matriz[elegido, pareja_e] +
                    matriz[elegido, pareja_o] - matriz[cambio, pareja_o]

                if Δ <= 0.0
                    apareo[i_elegido] = cambio
                    apareo[i_ocupado] = elegido
                    suma_distancias += Δ

                elseif rand() < exp(-Δ/T)
                    apareo[i_elegido] = cambio
                    apareo[i_ocupado] = elegido
                    suma_distancias += Δ
                end
            end
            n += 1
        end
    end
end

# Prepara la matriz de distancias y regresa la combinación con
# casi-mínima distancia meron-antimeron (SA).
# clusters = Índices tuplas
function apareamiento_minima_distancia_annealing_ma(clusters,
                                                    positivos::Vector{T},
                                                    negativos::Vector{T},
                                                    renglones_σ::T,
                                                    columnas_σ::T) where T<:Integer
    num_pos = length(positivos)
    num_neg = length(negativos)

    if num_pos <= num_neg
        matriz_distancias = zeros(num_pos, num_neg)
        for (i,p) in enumerate(positivos), (j,n) in enumerate(negativos)
            matriz_distancias[i,j] = distancia_menor(clusters[p], clusters[n],
                                                     renglones_σ, columnas_σ)
        end
        renglones = num_pos
        columnas = num_neg
    else
        matriz_distancias = zeros(num_neg, num_pos)
        for (i,n) in enumerate(negativos), (j,p) in enumerate(positivos)
            matriz_distancias[i,j] = distancia_menor(clusters[n],clusters[p],
                                                     renglones_σ, columnas_σ)
        end
        renglones = num_neg
        columnas = num_pos
    end

    if minimum(size(matriz_distancias)) < 11 # Hasta aquí los tiempos son comparables.
        minimo_absoluto = Float64[matriz_distancias[i,i] for i in 1:renglones]
        busca_apareamiento_minimo_ma!(  minimo_absoluto, copy(minimo_absoluto),
                                        matriz_distancias,
                                        collect(1.0:columnas), renglones, columnas)
        return minimo_absoluto
    else
        Tmax = 10.0
        nT = 50
        temperaturas = collect(linspace(Tmax,0.0, nT))

        repeticiones_annealing = 5 ### ¿Qué valor será el adecuado?
        minimos_relativos = Vector{Int64}[]
        sizehint!(minimos_relativos, repeticiones_annealing)

        # Repeticiones del algoritmo simulated annealing
        for busqueda in 1:repeticiones_annealing
            aleatorio = apareamiento_aleatorio_ma(matriz_distancias)
            annealing_ma!(  aleatorio, matriz_distancias, temperaturas,
                            10length(matriz_distancias))
            push!(minimos_relativos, aleatorio)
        end

        minimo = minimos_relativos[1]
        dist_min = sumador(minimo, matriz_distancias)

        for p in 2:repeticiones_annealing
            apareamiento = minimos_relativos[p]
            dist_apa = sumador(apareamiento, matriz_distancias)
            if dist_apa < dist_min
                minimo = apareamiento
                dist_min = dist_apa
            end
        end
        return traductor(minimo, matriz_distancias)
    end
end

# Prepara la matriz de distancias y regresa la combinación
# con casi-mínima distancia meron-meron (SA).
# cluster = Índices tuplas
function apareamiento_minima_distancia_annealing_mm(clusters,
                                                    cargados::Vector{T},
                                                    renglones_σ::T,
                                                    columnas_σ::T) where T<:Integer
    largo = length(cargados)
    matriz_distancias = zeros(largo, largo)
    for i in 1:largo, j in i+1:largo
        matriz_distancias[i,j] = distancia_menor(clusters[cargados[i]],
                                                 clusters[cargados[j]],
                                                 renglones_σ, columnas_σ)
        matriz_distancias[j,i] = matriz_distancias[i,j]
    end

    if largo % 2 == 0
        apareo_minimo = collect(1:largo)
    else
        apareo_minimo = collect(1:largo-1)
    end

    if largo < 17 # Hasta aquí los tiempos son comparables.

        apareo = Int64[]
        indices = collect(1:largo)
        falt = length(indices)

        busca_apareamiento_minimo_mm!(apareo_minimo, apareo, matriz_distancias,
                                      indices, largo)
        return medidor_individual(apareo_minimo, matriz_distancias)
    else
        Tmax = 10.0
        nT = 50
        temperaturas = collect(linspace(Tmax,0.0, nT))

        repeticiones_annealing = 5 ### ¿Qué valor será el adecuado?
        minimos_relativos = Vector{Int64}[]
        sizehint!(minimos_relativos, repeticiones_annealing)

        for busqueda in 1:repeticiones_annealing # Repeticiones del algoritmo simulated annealing
            apareo_anneal = apareamiento_aleatorio_mm(largo, length(apareo_minimo))
            annealing_mm!(apareo_anneal, matriz_distancias, temperaturas,
                          10length(matriz_distancias))
            push!(minimos_relativos, apareo_anneal)
        end

        minimo = minimos_relativos[1]
        dist_min = medidor_total(minimo, matriz_distancias)

        for p in 2:repeticiones_annealing
            apareamiento = minimos_relativos[p]
            dist_apa = medidor_total(apareamiento, matriz_distancias)
            if dist_apa < dist_min
                minimo = apareamiento
                dist_min = dist_apa
            end
        end
        return medidor_individual(minimo, matriz_distancias)
    end
end

# Anexa al diccionario las distancias correspondientes al
# apareamiento meron-antimeron (SA), para cada carga. (Índices tuplas)
function distancias_minimas_annealing_ma!(
            dict_cargas_distancias::Dict{<:Real, Vector{<:Real}},
            clusters, cargas::Vector{<:Real},
            Qmax::Real, renglones::T, columnas::T) where T<:Integer

    largo = length(cargas)
    matriz = zeros(Int64, largo, largo)

    #Se debe asegurar que esto no pasa afuera de la función.
    if Qmax == 0.0; return println("Configuración sin parejas."); end

    for (k,Q) in enumerate(0.5:0.5:Qmax)

        positivos, negativos = clusters_opuestos(Q, cargas)
        if length(positivos) == 0 || length(negativos) == 0
            println("No hay parejas con carga Q = $Q")
            continue
        end
        distancias_cuadradas = apareamiento_minima_distancia_annealing_ma(
                                clusters, positivos, negativos,renglones, columnas)
        registro = get!(dict_cargas_distancias, Q, Float64[])
        dict_cargas_distancias[Q] = append!(registro, sqrt(distancias_cuadradas))
    end
end

# Anexa al diccionario las distancias correspondientes al
# apareamiento meron-meron (SA), para cada carga. (Índices tuplas)
function distancias_minimas_annealing_mm!(
            dict_cargas_distancias::Dict{Float64, Tuple{Vector{<:Real},Vector{<:Real}}},
            clusters, cargas::Vector{<:Real},
            Qmax::Real, renglones::T, columnas::T) where T<:Integer

    #Se debe asegurar que esto no pasa afuera de la función.
    if Qmax == 0.0; return println("Configuración sin parejas."); end

    for (k,Q) in enumerate(0.5:0.5:Qmax)

        positivos, negativos = clusters_opuestos(Q, cargas)

        p = length(positivos)
        n = length(negativos)

        if p < 2
          if n < 2
            println("No hay suficientes elementos con carga Q = $Q")
            continue
          end
          distancias_cuadradas_negativos = apareamiento_minima_distancia_annealing_mm(
                                                clusters, negativos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[2], sqrt(distancias_cuadradas_negativos))
        elseif n < 2
          distancias_cuadradas_positivos = apareamiento_minima_distancia_annealing_mm(
                                                clusters, positivos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[1], sqrt(distancias_cuadradas_positivos))
        else
          distancias_cuadradas_positivos = apareamiento_minima_distancia_annealing_mm(
                                                clusters, positivos, renglones, columnas)
          distancias_cuadradas_negativos = apareamiento_minima_distancia_annealing_mm(
                                                clusters, negativos, renglones, columnas)
          registro = get!(dict_cargas_distancias, Q, (Float64[], Float64[]))
          append!(registro[1], sqrt(distancias_cuadradas_positivos))
          append!(registro[2], sqrt(distancias_cuadradas_negativos))
        end
    end
end

# Realiza una simulación multicluster_constrained y registra
# las distancias entre antipartículas (SA).
function distancia_annealing_ma!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Vector{Float64}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_ma!(diccionario, clusters, charges,
                                         Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{3, Float64}}()

    for k in keys(diccionario)
        veces = length(diccionario[k])
        # resultados[k] = (2veces/clusters_totales, mean(diccionario[k]),
        #                 std(diccionario[k])/sqrt(veces))
        resultados[k] = (veces, mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
    end
    resultados
end

# Lo mismo que el anterior pero con apareamientos entre elementos de la misma clase.
function distancia_annealing_mm!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_mm!(diccionario, clusters,  charges, Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{6, Float64}}()

    for k in keys(diccionario)
        pos = length(diccionario[k][1])
        neg = length(diccionario[k][2])
        resultados[k] = (pos, mean(diccionario[k][1]), std(diccionario[k][1])/sqrt(pos),
                         neg, mean(diccionario[k][2]), std(diccionario[k][2])/sqrt(neg))
    end
    resultados
end

# Unión de las dos funciones anteriores
function distancia_annealing!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                              ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario_ma = Dict{Float64, Vector{Float64}}()
    diccionario_mm = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_ma!(diccionario_ma, clusters,
                                         charges, Qmax, renglones, columnas)
        distancias_minimas_annealing_mm!(diccionario_mm, clusters,
                                         charges, Qmax, renglones, columnas)

        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{9, Float64}}()

    # Las llaves de ambos diccionarios no tienen porque coincidir...
    for k in keys(diccionario_mm)
        par = length(diccionario_ma[k])
        pos = length(diccionario_mm[k][1])
        neg = length(diccionario_mm[k][2])
        resultados[k] = (Float64(par), mean(diccionario_ma[k]),
                         std(diccionario_ma[k])/sqrt(pos),
                         Float64(pos), mean(diccionario_mm[k][1]),
                         std(diccionario_mm[k][1])/sqrt(pos),
                         Float64(neg), mean(diccionario_mm[k][2]),
                         std(diccionario_mm[k][2])/sqrt(neg))
    end
    resultados
end

# Realiza una simulación multicluster_semiconstrained y
# registra las distancias entre antipartículas (SA).
function distancia_annealing_ma!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Vector{Float64}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_ma!(diccionario, clusters,
                                         charges, Qmax,
                                         renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{3, Float64}}()

    for k in keys(diccionario)
        veces = length(diccionario[k])
        # resultados[k] = (2veces/clusters_totales,
        #                  mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
        resultados[k] = (veces, mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
    end
    resultados
end

# Lo mismo que el anterior pero con apareamientos entre
# elementos de la misma clase. (semiconstrained)
function distancia_annealing_mm!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_mm!(diccionario, clusters, charges,
                                        Qmax, renglones, columnas)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{6, Float64}}()

    for k in keys(diccionario)
        pos = length(diccionario[k][1])
        neg = length(diccionario[k][2])
        resultados[k] = (pos, mean(diccionario[k][1]), std(diccionario[k][1])/sqrt(pos),
                         neg, mean(diccionario[k][2]), std(diccionario[k][2])/sqrt(neg))
    end
    resultados
end

# Unión de las dos funciones anteriores. (semiconstrained)
function distancia_annealing!(  σ::AbstractMatrix{T}, pasos::Integer, δ::Real, β::Real
                                ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    Σ = copy(σ)
    μ = copy(σ)

    diccionario_ma = Dict{Float64, Vector{Float64}}()
    diccionario_mm = Dict{Float64, Tuple{Vector{Float64},Vector{Float64}}}()

    clusters_totales = 0  # No representan las parejas totales.
    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, carga_total = cargas!(σ, Σ, μ, clusters)

        Qmin, Qmax = extrema(charges)
        Qmax = min(abs(Qmin),abs(Qmax))
        if Qmax == 0.0
            println("Configuración sin parejas.")
            continue
        end

        distancias_minimas_annealing_ma!(diccionario_ma, clusters,
                                        charges, Qmax, renglones, columnas)
        distancias_minimas_annealing_mm!(diccionario_mm, clusters,
                                        charges, Qmax, renglones, columnas)

        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{9, Float64}}()

    for k in keys(diccionario_mm) # Las llaves de ambos diccionarios no tienen porque coincidir...
        par = length(diccionario_ma[k])
        pos = length(diccionario_mm[k][1])
        neg = length(diccionario_mm[k][2])
        resultados[k] = (   Float64(par), mean(diccionario_ma[k]),
                            std(diccionario_ma[k])/sqrt(pos),
                            Float64(pos), mean(diccionario_mm[k][1]),
                            std(diccionario_mm[k][1])/sqrt(pos),
                            Float64(neg), mean(diccionario_mm[k][2]),
                            std(diccionario_mm[k][2])/sqrt(neg))
    end
    resultados
end
