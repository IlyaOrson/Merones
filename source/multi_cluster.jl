# Almacena en una matriz auxiliar Σ los espínes de σ reflejados por el plano de Wolff.
function matriz_reflejada!( Σ::AbstractMatrix{T}, σ::AbstractMatrix{T}, direccion::T
                            ) where T<:AbstractVector{<:Real}
    for i in eachindex(σ)
        Σ[i] = σ[i] - 2(σ[i]⋅direccion)*direccion
    end
end

# Reflejar cada cluster con probabilidad 0.5. (Índices en tuplas)
function wang!( σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                clusters
                ) where T<:AbstractVector{<:Real}
    for cluster in clusters
        if rand() < 0.5
            for coord in cluster
                σ[coord...] = Σ[coord...]
            end
        end
    end
end

# Algoritmo Hoshen-Kopelman para obtener los clusters. (Modifica Σ)
function multicluster_standard!(σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                                direccion::T, β::Real
                                ) where T<:AbstractVector{<:Real}
   renglones, columnas = size(σ)
   matriz_reflejada!(Σ, σ, direccion) #Actualiza la matriz reflejada.
   Π = zeros(Int, renglones, columnas)
   etiquetador!(Π, σ, Σ, renglones, columnas, β, direccion)
   return clusters = mapa(Π)  #Vector{Vector{Tuple{Int64,Int64}}}
end

function multicluster_constrained!( σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                                    direccion::T, δ::Real
                                    ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    matriz_reflejada!(Σ, σ, direccion) #Actualiza la matriz reflejada.
    Π = zeros(Int, renglones, columnas)
    etiquetador!(Π, σ, Σ, renglones, columnas, δ)
    return clusters = mapa(Π)  #Vector{Vector{Tuple{Int64,Int64}}}
end

function multicluster_semiconstrained!( σ::AbstractMatrix{T}, Σ::AbstractMatrix{T},
                                        direccion::T, δ::Real, β::Real
                                        ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    matriz_reflejada!(Σ, σ, direccion) #Actualiza la matriz reflejada.
    Π = zeros(Int, renglones, columnas)
    etiquetador!(Π, σ, Σ, renglones, columnas, δ, β, direccion)
    return clusters = mapa(Π)  #Vector{Vector{Tuple{Int64,Int64}}}
end

# Evolución multicluster con acción estándar.
function evol_multi_standard!(  σ::AbstractMatrix{T}, pasos::Integer,
                                β::Real; graficos = "false"
                                ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    cargas_totales = zeros(pasos)
    Σ = copy(σ)
    μ = copy(σ)

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_standard!(σ, Σ, n, β)
        charges, origin = cargas!(σ, Σ, μ, clusters)
        cargas_totales[m] = origin

        if graficos == "cluster"
            coloreando_clusters(clusters, renglones,columnas)
        elseif graficos == "meron"
            coloreando_merones(clusters, charges, renglones,columnas)
        end
        wang!(σ, Σ, clusters)
    end
    if graficos == "false"
        mean(cargas_totales.^2)/(renglones*columnas), std(cargas_totales.^2)/sqrt(pasos)
    end
end

# Evolución multicluster con acción restringida.
function evol_multi_constrained!(   σ::AbstractMatrix{T}, pasos::Integer,
                                    δ::Real; graficos = "false"
                                    ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    cargas_totales = zeros(pasos)
    Σ = copy(σ)
    μ = copy(σ)

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        charges, origin = cargas!(σ, Σ, μ, clusters)
        cargas_totales[m] = origin

        if graficos == "cluster"
            coloreando_clusters(clusters, renglones,columnas)
        elseif graficos == "meron"
            coloreando_merones(clusters, charges, renglones,columnas)
        end
        wang!(σ, Σ, clusters)
    end
    if graficos == "false"
        mean(cargas_totales.^2)/(renglones*columnas), std(cargas_totales.^2)/sqrt(pasos)
    end
end

# Evolución multicluster con acción mixta.
function evol_multi_semiconstrained!(σ::AbstractMatrix{T}, pasos::Integer,
                                     δ::Real, β::Real;
                                     graficos = "false"
                                     ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    cargas_totales = zeros(pasos)
    Σ = copy(σ)
    μ = copy(σ)

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_semiconstrained!(σ, Σ, n, δ, β)
        charges, origin = cargas!(σ, Σ, μ, clusters)
        cargas_totales[m] = origin

        if graficos == "cluster"
            coloreando_clusters(clusters, renglones,columnas)
        elseif graficos == "meron"
            coloreando_merones(clusters, charges, renglones,columnas)
        end
        wang!(σ, Σ, clusters)
    end
    if graficos == "false"
        mean(cargas_totales.^2)/(renglones*columnas), std(cargas_totales.^2)/sqrt(pasos)
    end
end
