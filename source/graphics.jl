# Dibuja con distintos colores los clusters.
function coloreando_clusters(clusters, alto::T, ancho::T) where T<:Integer
    mat = zeros(alto,ancho)
    for (i, cluster) in enumerate(clusters)
        for coord in cluster
            mat[coord...] = i
        end
    end
    imshow(mat, origin = "lower", interpolation = "none", cmap="nipy_spectral")
    # hsv, prism, Set1, plasma, inferno, viridis, gist_ncar
    #colorbar(fraction = 0.045)
end

# Dibuja con distintos colores los clusters con carga distinta.
function coloreando_merones(clusters,
                            cargas::Vector{<:Real},
                            alto::T, ancho::T) where T<:Integer
    mat = zeros(alto,ancho)
    for (i, cluster) in enumerate(clusters)
        for coord in cluster
            mat[coord...] = cargas[i]
        end
    end
    c_min, c_max = extrema(cargas)
    partes = round(Int64, 2*(c_max-c_min)+1.0)

    imshow(mat, origin = "lower", cmap =  get_cmap("gnuplot", partes))
    title("Merons")

    #matshow(mat,origin = "lower", interpolation = "none", cmap =  plt[:cm][:get_cmap]("gnuplot", partes))
    #matshow(mat,origin = "lower", interpolation = "none",cmap="viridis")
    # hsv, prism, Set1, plasma, inferno, viridis, gist_ncar, nipy_spectral

    colorbar(fraction = 0.045, ticks=linspace(c_min,c_max,partes), label="Topological charges")
end

# Grafica el histograma de ocurrencias de clusters con cierta carga y el tamaño medio de estos.
function historiador(llavero::Dict{T, Tuple{T, T, T}}; graficar = "probabilidad") where T<:Real
    cargas = T[]
    ocurrencias = T[]
    medias = T[]
    errores = T[]
    for k in keys(llavero)
        push!(cargas, k)
        push!(ocurrencias, llavero[k][1])
        push!(medias, llavero[k][2])
        push!(errores, llavero[k][3])
    end

    if graficar == "probabilidad"
        bar(cargas, ocurrencias , width = 0.5)
        yscale("log")
        ylabel("Probability")
        xlabel("Topological charges from clusters")
        title("Logarithmic histogram")
        grid(true)
    elseif graficar == "tamaños"
        #bar(cargas-0.25, medias, width = 0.5, color = "b", yerr = errores, ecolor = "m", capsize = 8.0, log = true)
        errorbar(cargas, medias, errores, linestyle = "none", marker = ".", ecolor = "g", capsize = 2)
        min,max = extrema(cargas)
        xlim(min-0.5,max+0.5)
        xlabel("Topological charges from clusters")
        ylabel("Mean size")
        title("Mean sizes of clusters")
        grid(true)
    else
        error("graficar = \"probabilidad\" o \"tamaños\"")
    end
end

# Grafica el histograma de ocurrencias de cargas distintas y la distancas entre estas.
function historiador(llavero::Dict{T, Tuple{I, T, T}};
                    graficar = "distancias") where {I<:Integer,T<:Real}
    cargas = T[]
    ocurrencias = I[]
    medias = T[]
    errores = T[]
    for k in keys(llavero)
        push!(cargas, k)
        push!(ocurrencias, llavero[k][1])
        push!(medias, llavero[k][2])
        push!(errores, llavero[k][3])
    end

    if graficar == "ocurrencias"
        bar(cargas.-0.25, ocurrencias , width = 0.5)
        yscale("log")
        ylabel("Events")
        xlabel("Topological charges from clusters")
        title("Logarithmic histogram")
    elseif graficar == "distancias"
        bar(cargas-0.25, medias, width = 0.5, color = "b", yerr = errores,
            ecolor = "r", capsize = 8.0, log = true)
        min,max = extrema(cargas)
        xlim(min-0.5,max+0.5)
        xlabel("Topological charges from clusters")
        ylabel("Mean distance")
        grid(true)
    else
        error("graficar = \"ocurrencias\" o \"distancias\"")
    end
end

function historiador(llavero::Dict{T, Tuple{I, T, T, I, T, T}};
                    graficar = "distancias") where {I<:Integer, T<:Real}
    cargas = T[]
    ocurrencias_pos = I[]
    medias_pos = T[]
    errores_pos = T[]
    ocurrencias_neg = I[]
    medias_neg = T[]
    errores_neg = T[]
    for k in keys(llavero)
        push!(cargas, k)
        push!(ocurrencias_pos, llavero[k][1])
        push!(medias_pos, llavero[k][2])
        push!(errores_pos, llavero[k][3])
        push!(ocurrencias_neg, llavero[k][4])
        push!(medias_neg, llavero[k][5])
        push!(errores_neg, llavero[k][6])
    end

    if graficar == "ocurrencias"
        bar(cargas.-0.20, ocurrencias_pos, width = 0.20, color = "b", label = "Positivos")
        bar(cargas, ocurrencias_neg, width = 0.20, color = "g", label = "Negativos")
        legend()
        yscale("log")
        ylabel("Events")
        xlabel("Topological charges from clusters")
        title("Logarithmic histogram")
    elseif graficar == "distancias"
        bar(cargas-0.20, medias_pos, width = 0.20, color = "b", yerr = errores_pos,
            ecolor = "r", capsize = 8.0, label = "Positives")
        bar(cargas, medias_neg, width = 0.20, color = "g", yerr = errores_neg,
            ecolor = "m", capsize = 8.0, label = "Negatives")
        legend()
        min,max = extrema(cargas)
        xlim(min-0.5,max+0.5)
        xlabel("Topological charges from clusters")
        ylabel("Mean distance")
        yscale("log")
        grid(true)
    else
        error("graficar = \"ocurrencias\" o \"distancias\"")
    end
end

function historiador(datos::AbstractMatrix{Real}; graficar = "distancias")

    cargas = collect(datos[:,1])
    ocurrencias_pos = collect(datos[:,2])
    medias_pos = collect(datos[:,3])
    errores_pos = collect(datos[:,4])

    ancho = 0.5
    disp = 0.25

    MM = size(datos,2) > 4
    if MM
      ocurrencias_neg = collect(datos[:,5])
      medias_neg = collect(datos[:,6])
      errores_neg = collect(datos[:,7])
      ancho = 0.20
      disp = 0.20
    end

    if graficar == "ocurrencias"
        bar(cargas.-disp, ocurrencias_pos, width = ancho, color = "b", label = "Positivos")
        if MM
          bar(cargas, ocurrencias_neg, width = ancho, color = "g", label = "Negativos")
          legend()
        end
        #yscale("log")
        ylabel("Events")
        xlabel("Topological charges from clusters")
        #title("Histograma logarítmico")
    elseif graficar == "distancias"
        bar(cargas-disp, medias_pos, width = ancho, color = "b", yerr = errores_pos, ecolor = "r", capsize = 8.0, label = "Positives")
        if MM
          bar(cargas, medias_neg, width = ancho, color = "g", yerr = errores_neg, ecolor = "m", capsize = 8.0, label = "Negatives")
          legend()
        end
        min,max = extrema(cargas)
        xlim(min-0.5,max+0.5)
        xlabel("Topological charges from clusters")
        ylabel("Mean distance")
        #yscale("log")
        grid(true)
    else
        error("graficar = \"ocurrencias\" o \"distancias\"")
    end
end

# Gráfica lineal del descenso de la acción bajo
# la acción de Gradient Flow. (en pasos temporales)
function grafDescensoAccion(acciones, β)
    plot(acciones)
    xlabel("Time steps")
    ylabel("⟨s⟩")
    title("Action descent with GF (β = $β)")
    grid()
end

# Gráfica lineal del descenso de la acción bajo
# la acción de Gradient Flow. (escala de timepo real)
function grafDescensoAccion(tiempos, acciones, β)
    plot(tiempos, acciones)
    xlabel("Time")
    ylabel("⟨s⟩")
    title("Action descent with GF (β = $β)")
    grid()
end

# Gráfica lineal con errores en sus valores o
# relleno del área entre estos. (tiempo real con errores)
function grafDescensoAccion(tiempos, medias, errores, β; tipo = :fill)

    if tipo == :errorbar
        errorbar(tiempos, medias, yerr = errores,
                 linestyle = "dotted", marker = ".", mec = "orange",
                 ecolor = "orange", elinewidth = 1)
    elseif tipo == :fill
        plot(tiempos, medias, label = "Means")
        fill_between(tiempos, medias + errores, medias - errores,
                     facecolor = "yellow", edgecolor = "red", alpha = 0.4)
    else
        error("La variable \"tipo\" debe ser igual a \"errorbar\" o \"fill\".")
    end
    xlabel("Time steps")
    ylabel("⟨s⟩")
    title("Action evolution (β = $β)")
    grid()
end

# Gráfica lineal del ascenso cuasi-lineal de la acción bajo el rescalamiento t⟨s⟩.
function grafAscensoAccion(acciones, errores, h, α, β, L)

    tiempos, tS, tS_error = t_accion(acciones, errores, h)

    T0 = []
    try
        # Determinar el índice del tiempo en el que t⟨s⟩ = α
        # y los índices de las cotas de error
        inf, med, sup = sabueso(tS, tS_error, α)

        medio_t = tiempos[med]
        medio_c = tS[med]

        δt_inferior = medio_t - tiempos[inf]
        δt_superior = tiempos[sup] - medio_t
        δt = max(δt_superior, δt_inferior)

        t0 = reporta_error(medio_t, δt)
        push!(T0, t0)

        axvline(medio_t, alpha = 0.7, c = "k", ls = ":")
        axhline(medio_c, alpha = 0.7, c = "k")
        # annotate(string("t = ", reporta_error(medio_t,δt)),
        # xy=(medio_t + tiempos[end]/100, medio_c + tS[end]/100), xycoords = "data")

    catch mistake
        if isa(mistake, AssertionError)
            println(mistake)
            t0 = "-----"
            push!(T0, t0)
            axhline(α, alpha = 0.7, c = "k")
        else
            mistake
        end
    end
    t0 = T0[1]

    plot(tiempos, tS, label = "$L    $β    $t0")
    fill_between(tiempos, tS + tS_error, tS - tS_error, #label = "Error",
                 facecolor = "yellow", edgecolor = "red", alpha = 0.3)

    xlabel("flow time")
    title("Action evolution")
    grid()
    ylabel("⟨E⟩t")
    legend(title = "    L        β         to", prop=Dict("size"=>9))
end

function grafAjusteSlab(medias, errores, χ_ajustada, χ_error, volumen, confianza, β, Q;
                        tipo = :errorbar)

    datos = length(medias)
    columnas = datos-1
    dominio = (0:columnas) ./ columnas

    ajuste = parabola(dominio, volumen, χ_ajustada)

    if tipo == :errorbar #c = "magenta", mec = "orange", ecolor = "orange",
        ebar = errorbar(dominio, medias, yerr = errores,
                        ls = "none", marker = ".", markersize = 3,
                        capsize = 1.5, elinewidth = 1, label = "Means")
    elseif tipo == :fill
        fill_between(dominio, medias + errores, medias - errores, # label = "Error range",
                     facecolor = "yellow", edgecolor = "red", alpha = 0.4)
        ebar = plot(dominio, medias, ls = "dotted", label = "Means") # c = "magenta",
    else
        error("La variable \"tipo\" debe ser igual a \"errorbar\" o \"fill\".")
    end
    color_pasado = ebar[1][:get_color]()
    plot(   dominio, ajuste, color= color_pasado,
            label = string("Fit: χ = ", reporta_error(χ_ajustada, χ_error)))

    xlabel("Fraction of volume")
    ylabel(L"\langle q'\,^2\rangle")
    title("Fit with Slab Method (Q = $Q, β = $β)")
    grid()
    legend()
end

function grafAjusteSlab(medias, errores, χ_ajustada, χ_error, volumen, confianza, β, t, Q;
                        tipo = :errorbar)

    datos = length(medias)
    columnas = datos-1
    dominio = (0:columnas) ./ columnas

    ajuste = parabola(dominio, volumen, χ_ajustada)

    if tipo == :errorbar #c = "magenta", mec = "orange", ecolor = "orange",
        ebar = errorbar(dominio, medias, yerr = errores,
                        ls = "none", marker = ".", markersize = 3,
                        capsize = 1.5, elinewidth = 1)
    elseif tipo == :fill
        fill_between(dominio, medias + errores, medias - errores, # label = "Error range",
                     facecolor = "yellow", edgecolor = "red", alpha = 0.4)
        ebar = plot(dominio, medias, ls = "dotted") # c = "magenta",
    else
        error("La variable \"tipo\" debe ser igual a \"errorbar\" o \"fill\".")
    end
    color_pasado = ebar[1][:get_color]()
    plot(dominio, ajuste, color = color_pasado,
         label = string("χ = ", reporta_error(χ_ajustada, χ_error), "   t = ", round(t,5)))

    xlabel("Fraction of volume")
    ylabel(L"\langle q'\,^2\rangle")
    title("Fit with Slab Method (Q = $Q, β = $β)")
    grid()
    legend()
end

# Caso especial para ajuste de parábola corrida QCD
function grafAjusteSlab(medias, errores, χ_ajustada::Vector{<:Real},
                        χ_error::Vector{<:Real}, volumen, confianza,
                        β, t, Q; tipo = :errorbar)

    datos = length(medias)
    columnas = datos-1
    dominio = (0:columnas) ./ columnas

    ajuste = parabola_corrida(dominio, volumen, χ_ajustada)

    if tipo == :errorbar # c = "yellow", mec = "magenta", ecolor = "magenta"
        ebar = errorbar(dominio, medias, yerr = errores,
                        ls = "none", marker = ".", markersize = 3,
                        capsize = 1.5, elinewidth = 1)
    elseif tipo == :fill
        fill_between(dominio, medias + errores, medias - errores, # label = "Error range",
                     facecolor = "yellow", edgecolor = "red", alpha = 0.4)
        ebar = plot(dominio, medias, ls = "dotted") # c = "magenta",
    else
        error("La variable \"tipo\" debe ser igual a \"errorbar\" o \"fill\".")
    end
    color_pasado = ebar[1][:get_color]()
    plot(dominio, ajuste, color = color_pasado,
         label = string("χ = ", reporta_error(χ_ajustada[1], χ_error[1]),"  C = ",
         reporta_error(χ_ajustada[2], χ_error[2]),"   t = ", round(t,5)))

    xlabel("Fraction of volume")
    ylabel(L"\langle q'\,^2\rangle")
    title("Fit with Slab Method (Q = $Q, β = $β)")
    grid()
    legend()
end

# Grafica el ajuste de coseno hiperbólico para la correlación.
function grafCorr(promedios, errores, cutoff, β, confianza, t = 0)
    col = length(promedios) - 1
    subrango = cutoff + 1 : col + 1 - cutoff
    ξ_hint = col/6
    χ, χ_err = ajustador_cosh(promedios, subrango, confianza, ξ_hint)
    ξ, ξ_err = χ[2], χ_err[2]

    goodness = stdfit(promedios, errores, subrango, χ)

    rango = collect(0:col)
    rango_fino = linspace(0,col,1000)

    ebar = errorbar(rango, promedios, errores, marker = ".",
                    markersize = 3, capsize = 3, ls = "None")
    ajuste = kosh(rango_fino, col, χ)
    color = ebar[1][:get_color]()
    plot(rango_fino, ajuste, color = color, label = string(t, "   ", reporta_error(abs(ξ), ξ_err)))
    # plot(rango_fino, ajuste, color = color, label = string(β, "   ", reporta_error(abs(ξ), ξ_err)))
    # plot(rango_fino, ajuste, color = color, label = string(t,"  ", reporta_error(abs(ξ), ξ_err)))# ,"  χ/√df = ", round(goodness,3),"  L/ξ = ", reporta_error(col/ξ,col*ξ_err/ξ^2)))

    # println("t = ", round(t,4),"  ξ = ", reporta_error(abs(ξ), ξ_err) ,"  χ/√df = ", round(goodness,3),"  L/ξ = ", reporta_error(col/ξ,col*ξ_err/ξ^2))

    axvline(subrango[1]-1, alpha = 0.7, ls = "--", c = "b")
    axvline(subrango[end]-1, alpha = 0.7, ls = "--", c = "b")
    legend(title = "    t      ξ", prop=Dict("size"=>9))
    # legend(title = "   β      ξ")
    yscale("log")
    xlabel("r")
    ylabel("C(r)")
    title("correlation function (L = $col, β = $β)")
    # grid()
    return subrango, reporta_error(ξ, ξ_err), round(goodness,3), reporta_error(col/ξ,col*ξ_err/ξ^2), ξ, ξ_err
end

# julia> L, β = 120, 1.607
# julia> arch = Meron.jldopen("L$L.jld2", "r")
# julia> dict = arch["correlacion/sencilla/registro"]
# julia> for k in sorted_keys
#        mat = dict[k]
#        Meron.grafCorr(mat[:,1], mat[:,2], cld(L, 3), β, 0.95, k)
#        end
