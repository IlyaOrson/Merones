#Algoritmo single-cluster con acción estándar.
function volteo!(σ::AbstractMatrix{T}, pasos::Integer, β::Real) where T<:AbstractVector{<:Real}
    alto, ancho = size(σ)

    for i in 1:pasos

        r = rand_spin()
        coord = (rand(1:alto), rand(1:ancho))
        aglomerado = [coord]
        frontera = [coord]
        pared = 1
        #seguro = 0

        σ[coord...] -= 2(σ[coord...]⋅r)*r

        while pared != 0 #&& seguro<10000

            k = rand(1:pared)
            elegido = frontera[k]

            e = σ[elegido...]
            er = e⋅r

            for vecino in vecinos(elegido, alto, ancho)

                if !in(vecino, aglomerado)

                    v = σ[vecino...]
                    vr = v⋅r
                    ΔH = 2β*er*vr

                    if ΔH <= 0.0
                        if rand() < 1-exp(ΔH)
                            σ[vecino...] -= 2*(σ[vecino...]⋅r)*r
                            push!(aglomerado, vecino)
                            push!(frontera, vecino)
                            pared += 1
                        end
                    end
                end
            end
            deleteat!(frontera, k)
            pared -= 1
            #seguro += 1
        end
        # @show aglomerado
    end
end

function evol_single_standard!( σ::AbstractMatrix{T}, pasos::Integer, β::Real
                                ) where T<:AbstractVector{<:Real}
    n,m = size(σ)
    V = n*m

    cargas = zeros(pasos+1)
    carga_cluster = zeros(pasos)
    #cargas[1] = triangulador(σ)
    cargas[1] = round(2triangulador(σ))/2
    for i in 1:pasos
        volteo!(σ, 1, β)
        #cargas[i+1] = triangulador(σ)
        cargas[i+1] = round(2triangulador(σ))/2
        carga_cluster[i] = (cargas[i+1] - cargas[i])/2
    end
    cargas, carga_cluster, mean(cargas.^2)/V, std(cargas.^2)/sqrt(pasos+1)
end

## Constrained

##Algoritmo single-cluster con acción restringida.
function volteo_constrained!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                            ) where T<:AbstractVector{<:Real}
    tamaño = size(σ)
    alto = tamaño[1]
    ancho = tamaño[2]

    for i in 1:pasos

        r = rand_spin() #[1,0,0]
        coord = (rand(1:alto), rand(1:ancho))
        aglomerado = [coord]
        frontera = [coord]
        pared = 1
        #seguro = 0

        while (pared != 0) #& (seguro<10000)

            k = rand(1:pared)
            elegido = frontera[k]
            σ[elegido...] -= 2(σ[elegido...]⋅r)*r

            for vecino in vecinos(elegido, alto, ancho)

                if !in(vecino, aglomerado)

                    θ = acos( σ[vecino...] ⋅ σ[elegido...] ) #Ángulo tras voltear

                    if abs(θ) > δ
                        push!(aglomerado, vecino)
                        push!(frontera, vecino)
                        pared += 1
                    end
                end
            end
            deleteat!(frontera, k)
            pared -= 1
            #seguro += 1
        end
        # @show aglomerado
    end
end

function evol_single_constrained!(σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                                 ) where T<:AbstractVector{<:Real}
    n,m = size(σ)
    V = n*m

    cargas = zeros(pasos+1)
    carga_cluster = zeros(pasos)
    #cargas[1] = triangulador(σ)
    cargas[1] = round(2triangulador(σ))/2
    for i in 1:pasos
        volteo_constrained!(σ, 1, δ)
        #cargas[i+1] = triangulador(σ)
        cargas[i+1] = round(2triangulador(σ))/2
        carga_cluster[i] = (cargas[i+1] - cargas[i])/2
    end
    cargas, carga_cluster, mean(cargas.^2)/V, std(cargas.^2)/sqrt(pasos+1)
end
