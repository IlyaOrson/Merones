# params(runs, L, β, sweeps, terma, timeflow, timesteps, Δt0, ϵ)

# Genera un archivo con una configuración termalizada con los parámetros dados.
function generador_config(nombre_archivo, L, β, terma_sweeps)

    isfile(nombre_archivo) && error("¡El archivo $nombre_archivo ya existe!")

    σ = rand_spins(L, L)
    Σ = copy(σ)
    termalizador_standard!(σ, Σ, β, terma_sweeps)

    jldopen(nombre_archivo, "w") do archivo
        archivo["σ"] = σ
        archivo["Q"] = round(triangulador(σ))
        archivo["parametros/L"] = L
        archivo["parametros/β"] = β
        archivo["parametros/flowtime"] = round((L/6)^2 / 160, 4)
        archivo["parametros/terma_sweeps"] = terma_sweeps
    end
end

# Lee una configuración termalizada y ejecuta gradient flow con parámetros dados.
function recorder(nombre_archivo, flowsteps, initial_timesteps)

    @assert isfile(nombre_archivo) "¡El archivo $nombre_archivo no existe!"
    @assert isinteger(flowsteps) && flowsteps > 0

    jldopen(nombre_archivo, "r+") do arch

        σ0 = arch["σ"]
        L = arch["parametros/L"]
        β = arch["parametros/β"]
        flowtime = arch["parametros/flowtime"]

        for Δt0 in initial_timesteps

            σ = copy(σ0)
            Δt = Δt0

            σ_flowsteps = []
            acciones_flowsteps = []
            errores_flowsteps = []
            Q_flowsteps = []
            correlaciones_flowsteps = []
            elapsed_flowsteps = []

            for flowstep in 1:flowsteps

                to = flowtime * (flowsteps-1)
                tf = flowtime * flowsteps

                # Gradient flow con registros
                tic()
                acciones, errores = gradient_flow_action!(
                    σ, β, to, tf, Δt0; error_measure = phil_error
                    )
                elapsed = toq()

                # Cargas y correlaciones
                Q = round(triangulador(σ))
                correlaciones = mean_corr_single(σ)

                push!(σ_flowsteps, copy(σ))
                push!(acciones_flowsteps, acciones)
                push!(errores_flowsteps, errores)
                push!(Q_flowsteps, Q)
                push!(correlaciones_flowsteps, correlaciones)
                push!(elapsed_flowsteps, elapsed)
            end

            arch["RK4/Δt:$Δt0/flowsteps"] = flowsteps
            arch["RK4/Δt:$Δt0/σ"] = σ_flowsteps
            arch["RK4/Δt:$Δt0/Δt_inicial"] = Δt0
            arch["RK4/Δt:$Δt0/acciones"] = acciones_flowsteps
            arch["RK4/Δt:$Δt0/errores"] = errores_flowsteps
            arch["RK4/Δt:$Δt0/Q"] = Q_flowsteps
            arch["RK4/Δt:$Δt0/C(r)"] = correlaciones_flowsteps
            arch["RK4/Δt:$Δt0/cronometro"] = elapsed_flowsteps
        end
    end
end

# Lee una configuración termalizada y ejecuta gradient flow con parámetros dados.
function recorder(nombre_archivo, flowsteps, initial_timesteps, tolerances)

    @assert isfile(nombre_archivo) "¡El archivo $nombre_archivo no existe!"
    @assert isinteger(flowsteps) && flowsteps > 0

    jldopen(nombre_archivo, "r+") do arch

        σ0 = arch["σ"]
        L = arch["parametros/L"]
        β = arch["parametros/β"]
        flowtime = arch["parametros/flowtime"]

        # Dormad-Prince RK45
        for Δt0 in initial_timesteps
            for ϵ in tolerances

                σ = copy(σ0)
                Δt = Δt0

                σ_flowsteps = []
                Δt_flowsteps = []
                Δt_aceptados_flowsteps = []
                errores_flowsteps = []
                acciones_flowsteps = []
                errores_flowsteps = []
                Q_flowsteps = []
                correlaciones_flowsteps = []
                elapsed_flowsteps = []

                for flowstep in 1:flowsteps

                    to = flowtime * (flowsteps-1)
                    tf = flowtime * flowsteps

                    # Gradient flow con registros
                    tic()
                    Δt, acciones, Δt_aceptados, Δt_todos, errores =
                        adaptive_gradient_flow_action!(
                            σ, β, to, tf, Δt; error_measure = phil_error, ε = ϵ
                            )
                    elapsed = toq()

                    # Cargas y correlaciones
                    Q = round(abs(triangulador(σ)))
                    correlaciones = mean_corr_single(σ)

                    push!(σ_flowsteps, copy(σ))
                    push!(Δt_flowsteps, Δt)
                    push!(Δt_aceptados_flowsteps, Δt_aceptados)
                    push!(acciones_flowsteps, acciones)
                    push!(errores_flowsteps, errores)
                    push!(Q_flowsteps, Q)
                    push!(correlaciones_flowsteps, correlaciones)
                    push!(elapsed_flowsteps, elapsed)
                end

                arch["RK45/Δt:$Δt0/tol:$ϵ/flowsteps"] = flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/σ"] = σ_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/Δt_inicial"] = Δt0
                arch["RK45/Δt:$Δt0/tol:$ϵ/Δt_final"] = Δt_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/Δt_aceptados"] = Δt_aceptados_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/acciones"] = acciones_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/errores"] = errores_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/Q"] = Q_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/C(r)"] = correlaciones_flowsteps
                arch["RK45/Δt:$Δt0/tol:$ϵ/cronometro"] = elapsed_flowsteps
            end
        end
    end
end

# Recolectar y reconstruir valores a partir de
function recollect(measurements)
    discrete = measurements[1][[1,end]]
    complete = measurements[1]
    for i in 2:length(measurements)
        push!(discrete, measurements[i][end])
        append!(complete, measurements[i][2:end])
    end
    return discrete, complete
end

#=
@time Meron.generador_config("hola.jld2", 80, 1.535, 10000)
@time Meron.recorder("hola.jld2", 2, [0.0001], [1E-6, 1E-7])
=#
