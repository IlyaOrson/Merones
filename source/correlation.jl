# Calcula el promedio de los spins sobre la columna c de la configuración σ.
function mean_spin_col(σ::AbstractMatrix{T}, c) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    suma = zero(eltype(σ))
    for i in 1:renglones
        suma += σ[i,c]
    end
    return suma #./ renglones Esto se hace después...
end

# Calcula el promedio de los spins sobre el renglón r de la configuración σ.
function mean_spin_row(σ::AbstractMatrix{T}, r) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    suma = zero(eltype(σ))
    for j in 1:columnas
        suma += σ[r,j]
    end
    return suma #./ columnas Esto se hace después...
end

# Actualiza el vector medias con las medias (sin dividir aún)
# de spins sobre columnas de la configuración actual σ
function mean_spin_cols!(σ::AbstractMatrix{T}, medias) where T<:AbstractVector{<:Real}
    # @assert length(medias) == size(σ,2)
    columnas = length(medias)
    for j in 1:columnas
        medias[j] = mean_spin_col(σ, j)
    end
end

# Actualiza el vector medias con las medias (sin dividir aún)
# de spins sobre columnas de la configuración actual σ
function mean_spin_rows!(σ::AbstractMatrix{T}, medias) where T<:AbstractVector{<:Real}
    # @assert length(medias) == size(σ,1)
    renglones = length(medias)
    for i in 1:renglones
        medias[i] = mean_spin_row(σ, i)
    end
end

function mean_corr_single(σ::AbstractMatrix{T}) where T<:AbstractVector{<:Real}
    rows, cols = size(σ)
    r2 = rows^2

    medias = zeros(eltype(σ), cols)
    correlaciones = zeros(cols)

    mean_spin_cols!(σ, medias)

    s1 = medias[1]
    for c in 1:cols
        s2 = medias[c]
        s12_medio = (s1 ⋅ s2) ./ r2 # dividir cada suma por columnas (s) entre renglones
        correlaciones[c] = s12_medio
    end
    return correlaciones
end

function mean_corr_single!( σ::AbstractMatrix{T}, medias, corr_medias::AbstractMatrix{<:Real}
                            ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    ren2 = renglones^2

    mean_spin_cols!(σ, medias)

    s1 = medias[1]
    for c in 1:columnas
        s2 = medias[c]
        s12_medio = (s1 ⋅ s2) ./ ren2
        corr_medias[1,c] += s12_medio
        corr_medias[2,c] += s12_medio^2
    end
end

# Agrega la contribución de todas las posibles configuraciones (#columnas x #renglones)
function mean_corr_all!(σ::AbstractMatrix{T}, medias, corr_medias::AbstractMatrix{<:Real}
                        ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)

    ren2 = renglones^2
    col2 = columnas^2

    mean_spin_cols!(σ, medias)
    rango_columnas = 1:columnas

    for c1 in rango_columnas # Indica el punto inicial
        s1 = medias[c1] # Media sobre primera columna
        for C in rango_columnas
            ρ = C + c1 - 1
            ρ > columnas ? c = ρ - columnas : c = ρ
            sx = medias[c] # Media sobre columna c
            s1x = s1 ⋅ sx
            s1x_medio = s1x ./ ren2
            corr_medias[1,C] += s1x_medio
            corr_medias[2,C] += s1x_medio^2
        end
    end

    mean_spin_rows!(σ, medias)
    rango_renglones = 1:renglones

    for r1 in rango_renglones # Indica el punto inicial
        s1 = medias[r1] # Media sobre primer renglón
        for R in rango_renglones
            ρ = R + r1 - 1
            ρ > renglones ? r = ρ - renglones : r = ρ
            sx = medias[r]
            s1x = s1 ⋅ sx
            s1x_medio = s1x ./ col2
            corr_medias[1,R] += s1x_medio
            corr_medias[2,R] += s1x_medio^2
        end
    end
end

# Función para determinar la longitud de correlación
# en una configuración con fronteras periódicas
function kosh(x, L, χ)
    A, ξ = χ
    num = x .- L/2
    A .* cosh.(num./ξ)
    # @. A * cosh( (x - L/2)/ξ )
end

# Solamente utilizar los datos dentro del rango y proporcionar ayuda para convergencia
function ajustador_cosh(promedios, subrango, confianza::AbstractFloat, ξ_hint = 1.0)

    @assert promedios[1] == promedios[end] "El primer y último promedio deberían ser el mismo (#datos = L + 1)."

    tamaño = length(promedios)
    dominio = subrango .- 1 # transformar los índices al verdadero dominio de los datos
    subpromedios = promedios[subrango]

    L = tamaño - 1
    modelo(x, χ) = kosh(x, L, χ)
    ajuste = curve_fit(modelo, dominio, subpromedios, [0.1, ξ_hint])

    χ_ajustada = ajuste.param
    χ_error = estimate_errors(ajuste, confianza)

    return χ_ajustada, χ_error # Para evitar obtener un arreglo de una sola dimensión
end

ajustador_cosh(promedios, confianza) = ajustador_cosh(  promedios,
                                                        eachindex(promedios),
                                                        confianza)

function ajustador_cosh(promedios, errores, cutoff::Integer, confianza)
    col = length(promedios) - 1
    subrango = cutoff + 1 : col + 1 - cutoff
    ξ_hint = col/6
    χ, χ_err = ajustador_cosh(promedios, subrango, confianza, ξ_hint)
    ξ, ξ_err = χ[2], χ_err[2]
    goodness = stdfit(promedios, errores, subrango, χ)
    return abs(ξ), ξ_err, goodness
end

# Encuentra STDFIT = sqrt(WSSR/NDF) (following gnuplot)
# Weighted sum-of-squares residuals
# WSSR = ∑ [(y_i - f(x_i)) / SD_i]^2  f = fitted_function
# Standard error of the mean
# SE = SD/√n
# Number of degrees of freedom
# NDF = #data - #parameters
function stdfit(promedios, errores, subrango, χ_ajustada)

    @assert promedios[1] == promedios[end] "El primer y último promedio deberían ser el mismo (#datos = L + 1)."

    tamaño = length(promedios)
    parametros = length(χ_ajustada)
    ndf = tamaño - parametros

    L = tamaño - 1
    modelo(x) = kosh(x, L, χ_ajustada)

    wssr = 0.0
    for i in subrango
        wssr += ( (promedios[i] - modelo(i-1)) / errores[i] )^2
    end
    return sqrt(wssr/ndf)
end
