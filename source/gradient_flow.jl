# Obtiene la discretización pertinente al laplaciano en el
# lugar x,y de la matriz para la componente i del espin.
function diferencias_finitas(σ::AbstractMatrix{T}, x, y, i) where T<:AbstractVector{<:Real}
    # Condiciones periódicas
    renglones, columnas = size(σ)
    x == 1 ? x_menor = renglones : x_menor = x-1
    y == 1 ? y_menor = columnas  : y_menor = y-1
    x == renglones ? x_mayor = 1 : x_mayor = x+1
    y == columnas  ? y_mayor = 1 : y_mayor = y+1

    dSi = 0.0
    Si = σ[x,y][i]

    for j in 1:3
        Sj = σ[x,y][j]
        Sj_x_mayor = σ[x_mayor, y][j]
        Sj_x_menor = σ[x_menor, y][j]
        Sj_y_mayor = σ[x, y_mayor][j]
        Sj_y_menor = σ[x, y_menor][j]

        i == j ? δ = 1 : δ = 0

        dSi += (δ - Si*Sj) * (Sj_x_mayor + Sj_x_menor + Sj_y_mayor + Sj_y_menor - 4.0*Sj)
    end
    return dSi
end

# Genera la matriz con las derivadas temporales. (Independiente del tiempo en realidad)
function derivadas_temporales!( t, σ::AbstractMatrix{T}, dσ::AbstractMatrix{T}
                                ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    for y in 1:columnas, x in 1:renglones
        s1 = diferencias_finitas(σ, x, y, 1)
        s2 = diferencias_finitas(σ, x, y, 2)
        s3 = diferencias_finitas(σ, x, y, 3)
        dσ[x,y] = SVector(s1,s2,s3)
    end
end

# Calcula la norma media de una configuración (para calcular la norma de la derivada)
function norma_media(dσ)
    Σ = 0.0
    N = length(dσ)
    for i in eachindex(dσ)
        Σ += norm(dσ[i])
    end
    Σ/N
end

# Normaliza todos los espines en la configuración.
function normalizador!(σ::AbstractMatrix{T}) where T<:AbstractVector{<:Real}
    for i in eachindex(σ)
        σ[i] /= norm(σ[i])
    end
end

# Funciones auxiliares necesarias para la versión 0.5
sum_mul(σ, h, k) = σ + h * k
paso_rk4(σ, h, k1, k2, k3, k4) = σ + h * ( k1 + 2k2 + 2k3 + k4 ) / 6

# Algoritmo de Runge-Kutta clásico (preallocation).
function RK4!(t, σ, f!, h, k1, k2, k3, k4, σ_h2_k1, σ_h2_k2, σ_h_k3)

    h2 = h/2

    f!(t, σ, k1)

    # @. σ_h2_k1 = σ + h2 * k1
    broadcast!(sum_mul, σ_h2_k1, σ, h2, k1)
    f!(t+h2, σ_h2_k1, k2)

    # @. σ_h2_k2 = σ + h2 * k2
    broadcast!(sum_mul, σ_h2_k2, σ, h2, k2)
    f!(t+h2, σ_h2_k2, k3)

    # @. σ_h_k3 = σ + h * k3
    broadcast!(sum_mul, σ_h_k3, σ, h, k3)
    f!(t+h, σ_h_k3, k4)

    # @. σ += h*(k1+2k2+2k3+k4)/6
    broadcast!(paso_rk4, σ, σ, h, k1, k2, k3, k4)
    normalizador!(σ)
end

# Runge-Kutta Dormand-Prince "Numerica Recipes"
sum_mul_1(σ, h, a1, k1) =
    σ + h * k1
sum_mul_2(σ, h, a1,a2, k1,k2) =
    σ + h*(a1*k1 + a2*k2)
sum_mul_3(σ, h, a1,a2,a3, k1,k2,k3) =
    σ + h*(a1*k1 + a2*k2 + a3*k3)
sum_mul_4(σ, h, a1,a2,a3,a4, k1,k2,k3,k4) =
    σ + h*(a1*k1 + a2*k2 + a3*k3 + a4*k4)
sum_mul_5(σ, h, a1,a2,a3,a4,a5, k1,k2,k3,k4,k5) =
    σ + h*(a1*k1 + a2*k2 + a3*k3 + a4*k4 + a5*k5)
sum_mul_6(σ, h, a1,a2,a3,a4,a5,a6, k1,k2,k3,k4,k5,k6) =
    σ + h*(a1*k1 + a2*k2 + a3*k3 + a4*k4 + a5*k5 + a6*k6)

# Numerical recipes in C suggestion
function recipes_error(Ω, ω)
    Δ = Ω - ω
    N = length(Δ)
    Σ = 0.0
    for i in eachindex(Δ)
        Δi = Δ[i]
        Σ += Δi ⋅ Δi
    end
    sqrt( Σ/N )
end

# Phillipe's suggestion
function phil_error(Ω, ω)
    N = length(Ω)
    Σ = Float64(N)
    for i in eachindex(Ω)
        s = ω[i]
        S = Ω[i]
        Σ -= (s ⋅ S)^2
    end
    Σ/N
end

# Cotas para el valor del salto temporal
function acotador(h_new, h_old, maxscale=10, minscale=0.2)
    rel = h_new / h_old
    if rel > maxscale
        h = h_old*maxscale
    elseif rel < minscale
        h = h_old*minscale
    else
        h = h_new
    end
end

# Sugiere un nuevo paso de integración e indica si se debe repetir la integración
function new_time_step( h, tol, errata;
                        safe = 0.85, expo = 0.2, #^(1/5)
                        maxscale = 10, minscale = 0.2)
    if errata <= 0.0
        return maxscale * h, false # h_new = maxscale * h
    end
    frac = tol/errata
    H = safe * h * frac^expo
    H = acotador(H, h, maxscale, minscale)
    repeat = frac < 1 ? true : false
    return H, repeat
end

function RK45!( t, σ, f!, h, ε, ω, Ω,
                k1, k2, k3, k4, k5, k6,
                σ_k2, σ_k3, σ_k4, σ_k5, σ_k6,
                c2,c3,c4,c5,c6,c7,
                a21,a31,a32,a41,a42,a43,a51,a52,a53,a54,
                a61,a62,a63,a64,a65,a71,a72,a73,a74,a75,a76,
                b1,b2,b3,b4,b5,b6,b7,
                B1,B2,B3,B4,B5,B6,B7;
                error_measure = phil_error)

    f!(t, σ, k1) # k₁ = f(xₙ, yₙ)

    broadcast!( sum_mul_1, σ_k2, σ, h,
                a21, k1)
    f!(t + c2*h, σ_k2, k2) # k₂ = f(xₙ + c₂, yₙ + a₂₁k₁)

    broadcast!( sum_mul_2, σ_k3, σ, h,
                a31, a32, k1, k2)
    f!(t + c3*h, σ_k3, k3) # k₃

    broadcast!( sum_mul_3, σ_k4, σ, h,
                a41, a42, a43, k1, k2, k3)
    f!(t + c4*h, σ_k4, k4) # k₄

    broadcast!( sum_mul_4, σ_k5, σ, h,
                a51, a52, a53, a54, k1, k2, k3, k4)
    f!(t + c5*h, σ_k5, k5) # k₅

    broadcast!( sum_mul_5, σ_k6, σ, h,
                a61, a62, a63, a64, a65, k1, k2, k3, k4, k5)
    f!(t + c6*h, σ_k6, k6) # k₆

    # y*ₙ₊₁ (fourth-order)
    broadcast!( sum_mul_6, ω, σ, h,
                B1,B2,B3,B4,B5,B6, k1,k2,k3,k4,k5,k6)
    normalizador!(ω)

    # yₙ₊₁ (fifth-order)
    broadcast!( sum_mul_6, Ω, σ, h,
                b1,b2,b3,b4,b5,b6, k1,k2,k3,k4,k5,k6)
    normalizador!(Ω)

    # Tomar como referencia el valor de la derivada
    tol = ε * h * norma_media(k1)
    errata = error_measure(Ω, ω)
    return H, repeat = new_time_step(h, tol, errata)
end

# Integrador.
function gradient_flow!(σ::AbstractMatrix{T}, β, to, tf, h
                        ) where T<:AbstractVector{<:Real}

    tiempos = to : h : tf

    k1 = copy(σ)
    k2 = copy(σ)
    k3 = copy(σ)
    k4 = copy(σ)
    σ_h2_k1 = copy(σ)
    σ_h2_k2 = copy(σ)
    σ_h_k3 = copy(σ)

    for i in 1:length(tiempos)-1
        t = tiempos[i]
        RK4!(t, σ, derivadas_temporales!, h, k1, k2, k3, k4, σ_h2_k1, σ_h2_k2, σ_h_k3)
    end
    return h
end

# Integrador que registra la evolución de la acción en cada paso.
function gradient_flow_action!( σ::AbstractMatrix{T}, β, to, tf, h;
                                error_measure = phil_error
                                ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    tiempos = to : h : tf

    errores = zeros(length(tiempos)-1)
    S = zeros(tiempos)
    S[1] = accion(σ, β)

    γ = copy(σ)
    k1 = copy(σ)
    k2 = copy(σ)
    k3 = copy(σ)
    k4 = copy(σ)
    σ_h2_k1 = copy(σ)
    σ_h2_k2 = copy(σ)
    σ_h_k3 = copy(σ)

    for i in 1:length(tiempos)-1
        t = tiempos[i]
        RK4!(t, σ, derivadas_temporales!, h, k1, k2, k3, k4, σ_h2_k1, σ_h2_k2, σ_h_k3)

        errores[i] = error_measure(σ, γ)
        S[i+1] = accion(σ, β)
        γ .= σ
    end
    return S, errores
end

# Integrador adaptativo RK45 con coeficientes de Dormand-Prince
function adaptive_gradient_flow!(   σ::AbstractMatrix{T}, β, to, tf, h;
                                    error_measure = phil_error,
                                    ε = 1E-6
                                    ) where T<:AbstractVector{<:Real}

    # Dormand-Prince Butcher tableau
    c2 = 1/5
    c3 = 3/10
    c4 = 4/5
    c5 = 8/9
    c6 = 1.
    c7 = 1.

    a21 = 1/5
    a31 = 3/40
    a32 = 9/40
    a41 = 44/45
    a42 = -56/15
    a43 = 32/9
    a51 = 19372/6561
    a52 = -25360/2187
    a53 = 64448/6561
    a54 = -212/729
    a61 = 9017/3168
    a62 = -355/33
    a63 = 46732/5247
    a64 = 49/176
    a65 = -5103/18656
    a71 = 35/384
    a72 = 0.
    a73 = 500/1113
    a74 = 125/192
    a75 = -2187/6784
    a76 = 11/84

    b1 = 35/384
    b2 = 0.
    b3 = 500/1113
    b4 = 125/192
    b5 = -2187/6784
    b6 = 11/84
    b7 = 0.

    B1 = 5179/57600
    B2 = 0.
    B3 = 7571/16695
    B4 = 393/640
    B5 = -92097/339200
    B6 = 187/2100
    B7 = 1/40

    # Arreglos auxiliares donde se almacenará los pasos intermedios de RK45
    k1 = copy(σ)
    k2 = copy(σ)
    k3 = copy(σ)
    k4 = copy(σ)
    k5 = copy(σ)
    k6 = copy(σ)

    σ_k2 = copy(σ)
    σ_k3 = copy(σ)
    σ_k4 = copy(σ)
    σ_k5 = copy(σ)
    σ_k6 = copy(σ)

    ω = copy(σ)
    Ω = copy(σ)

    t = to
    while true
        h, repeat = RK45!(  t, σ, derivadas_temporales!, h, ε, ω, Ω,
                            k1, k2, k3, k4, k5, k6,
                            σ_k2, σ_k3, σ_k4, σ_k5, σ_k6,
                            c2,c3,c4,c5,c6,c7,
                            a21,a31,a32,a41,a42,a43,a51,a52,a53,a54,
                            a61,a62,a63,a64,a65,a71,a72,a73,a74,a75,a76,
                            b1,b2,b3,b4,b5,b6,b7,
                            B1,B2,B3,B4,B5,B6,B7;
                            error_measure = error_measure)
        if !repeat
            t += h
            σ .= Ω
            # revisar si el siguiente paso sobrepasará el tiempo final
            # y entonces dar sólo el paso necesario para terminar
            if t + h > tf
                hh = tf-t
                RK4!(t, σ, derivadas_temporales!, hh, k1, k2, k3, k4, σ_k2, σ_k3, σ_k4)
                # devolver el paso temporal sugerido para no repetir ajuste inicial
                return h
            end
        end
    end
end

# Integrador adaptativo RK45 con coeficientes de Dormand-Prince
function adaptive_gradient_flow_action!(σ::AbstractMatrix{T}, β, to, tf, h;
                                        error_measure = phil_error,
                                        ε = 1E-6
                                        ) where T<:AbstractVector{<:Real}

    # Dormand-Prince Butcher tableau
    c2 = 1/5
    c3 = 3/10
    c4 = 4/5
    c5 = 8/9
    c6 = 1.
    c7 = 1.

    a21 = 1/5
    a31 = 3/40
    a32 = 9/40
    a41 = 44/45
    a42 = -56/15
    a43 = 32/9
    a51 = 19372/6561
    a52 = -25360/2187
    a53 = 64448/6561
    a54 = -212/729
    a61 = 9017/3168
    a62 = -355/33
    a63 = 46732/5247
    a64 = 49/176
    a65 = -5103/18656
    a71 = 35/384
    a72 = 0.
    a73 = 500/1113
    a74 = 125/192
    a75 = -2187/6784
    a76 = 11/84

    b1 = 35/384
    b2 = 0.
    b3 = 500/1113
    b4 = 125/192
    b5 = -2187/6784
    b6 = 11/84
    b7 = 0.

    B1 = 5179/57600
    B2 = 0.
    B3 = 7571/16695
    B4 = 393/640
    B5 = -92097/339200
    B6 = 187/2100
    B7 = 1/40

    # Arreglos auxiliares donde se almacenará los pasos intermedios de RK45
    k1 = copy(σ)
    k2 = copy(σ)
    k3 = copy(σ)
    k4 = copy(σ)
    k5 = copy(σ)
    k6 = copy(σ)

    σ_k2 = copy(σ)
    σ_k3 = copy(σ)
    σ_k4 = copy(σ)
    σ_k5 = copy(σ)
    σ_k6 = copy(σ)

    ω = copy(σ)
    Ω = copy(σ)

    # Registro de acción
    S = Float64[]
    push!(S, accion(σ, β))

    # Registro de tiempos
    t_aceptados = Float64[]
    t_todos = Float64[]
    push!(t_todos, h)

    # Registro de errores
    errores = Float64[]

    t = to
    while true
        h, repeat = RK45!(  t, σ, derivadas_temporales!, h, ε, ω, Ω,
                            k1, k2, k3, k4, k5, k6,
                            σ_k2, σ_k3, σ_k4, σ_k5, σ_k6,
                            c2,c3,c4,c5,c6,c7,
                            a21,a31,a32,a41,a42,a43,a51,a52,a53,a54,
                            a61,a62,a63,a64,a65,a71,a72,a73,a74,a75,a76,
                            b1,b2,b3,b4,b5,b6,b7,
                            B1,B2,B3,B4,B5,B6,B7;
                            error_measure = error_measure)
        push!(t_todos, h)
        if !repeat

            push!(errores, error_measure(σ, Ω))

            t += h
            σ .= Ω

            # registro de acción
            push!(S, accion(σ, β))
            push!(t_aceptados, h)

            # revisar si el siguiente paso sobrepasará el tiempo final
            if t + h > tf
                hh = tf-t # dar solamente el paso necesario
                RK4!(t, σ, derivadas_temporales!, hh, k1, k2, k3, k4, σ_k2, σ_k3, σ_k4)

                # registro de acción
                push!(S, accion(σ, β))

                # devolver el paso temporal sugerido para no repetir ajuste inicial
                return h, S, t_aceptados, t_todos, errores
            end
        end
    end
end

adaptive_gradient_flow!(rand_spins(2,2), 1.0, 0.0, 0.1, 0.1) # compilation
adaptive_gradient_flow_action!(rand_spins(2,2), 1.0, 0.0, 0.1, 0.1) # compilation

# Ejecuta el algoritmo multicluster estándar β, y cada "intersweeps" lleva
# a cabo una corrida Gradient Flow. Registra un total de "cuantos_GF" de estas
# corridas, donde cada una registra el descenso en la acción durante cada corrida.
function sporadicGF(σ::AbstractMatrix{T},
                    intersweeps::Integer,
                    cuantos_GF::Integer,
                    β, to, tf, h) where T<:AbstractVector{<:Real}
    Σ = copy(σ)
    termalizacion = 10^6
    termalizador_standard!(σ, Σ, β, termalizacion) # Termalización

    numero_acciones = length(to:h:tf)
    matriz_acciones = zeros(numero_acciones, cuantos_GF)
    congelado = copy(σ)

    for i in 1:cuantos_GF
        termalizador_standard!(σ, Σ, β, intersweeps)
        congelado .= σ
        reg_acciones = gradient_flow_action!(congelado, β, to, tf, h)
        matriz_acciones[:,i] = reg_acciones
    end
    return matriz_acciones
end

# Calcula la correlación y la susceptibilidad magnética en tiempos multiplos
# del time-flow sin almacenar datos intermedios para la correlación.
function sliced_correlationGF(  σ::AbstractMatrix{T},
                                intersweeps::Integer,
                                cuantos_GF::Integer, to,
                                flow_steps::Integer, h, β;
                                adaptive::Bool = true) where T<:AbstractVector{<:Real}
    renglones, columnas = size(σ)
    renglones == columnas || error("La configuración debe ser cuadrada.")
    volumen = length(σ)
    Σ = copy(σ)

    termalizacion = 10^6
    termalizador_standard!(σ, Σ, β, termalizacion) # Termalización

    mediciones_medias = cuantos_GF
    registro_correlaciones_medias = Dict(n*to => zeros(2,columnas) for n in 0:flow_steps)
    medias_espines = zeros(eltype(σ), columnas)
    registro_cargas = zeros(flow_steps + 1, 2) # Registro para calcular χ = ⟨Q^2⟩/V
    congelado = copy(σ)

    if adaptive
        GF! = adaptive_gradient_flow!
    else
        GF! = gradient_flow!
    end

    for i in 1:cuantos_GF
        termalizador_standard!(σ, Σ, β, intersweeps)

        # Método alternativo que utiliza la media del spin sobre columnas
        mean_corr_single!(σ, medias_espines, registro_correlaciones_medias[0.0])

        registra_carga!(registro_cargas, σ, 0)

        congelado .= σ
        for n in 1:flow_steps
            nt_ = (n-1)*to
            nt = n*to

            h = GF!(congelado, β, nt_, nt, h)

            # Método alternativo que utiliza la media del spin sobre columnas
            mean_corr_single!(congelado, medias_espines, registro_correlaciones_medias[nt])

            registra_carga!(registro_cargas, congelado, n)
        end
    end

    # Calcular medias y errores a partir de la matriz de sumas para cada flow step
    for p in 0:flow_steps
        k = p*to
        reg_corr_medias = registro_correlaciones_medias[k]
        medias_errores_renglones!(reg_corr_medias, mediciones_medias)
    end

    # Calcular medias y errores a partir de la matriz de sumas
    medias_errores_columnas!(registro_cargas, cuantos_GF, volumen) # χ y χ_err

    return registro_correlaciones_medias, registro_cargas
end
