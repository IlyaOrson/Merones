## ¿La carga y el tamaño de una isla están relacionadas? -- Sí
function tamaño_islas!( diccionario::Dict,
                        clusters, cargas::Vector{<:Real})
    for (i, (carguita, cluster)) in enumerate(zip(cargas, clusters))
        if haskey(diccionario, carguita)
            push!(diccionario[carguita], length(cluster))
        else
            diccionario[carguita] = [length(cluster)]
        end
    end
end

function carga_tamaño!( σ::AbstractMatrix{T}, pasos::Integer, δ::Real
                        ) where T<:AbstractVector{<:Real}

    renglones, columnas = size(σ)
    # carga => [tamaño_cluster 1, 2 , ...]
    diccionario = Dict{Float64, Vector{Int64}}()
    clusters_totales = 0
    Σ = copy(σ)
    μ = copy(σ)

    for m in 1:pasos
        n = rand_spin()
        clusters = multicluster_constrained!(σ, Σ, n, δ)
        cargas_clusters, carga_total = cargas!(σ, Σ, μ, clusters)
        tamaño_islas!(diccionario, clusters, cargas_clusters)
        wang!(σ, Σ, clusters)
        clusters_totales += length(clusters)
    end

    resultados = Dict{Float64, NTuple{3,Float64}}()

    for k in keys(diccionario)
        veces = length(diccionario[k])
        resultados[k] = (veces/clusters_totales, mean(diccionario[k]), std(diccionario[k])/sqrt(veces))
    end
    resultados
end
