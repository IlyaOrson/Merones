# Identifica los clusters con carga opuesta.
# La carga debe ser no negativa.
function clusters_opuestos(carga::T, cargas::Vector{T}) where T<:Real
    positivos = Int64[]
    negativos = Int64[]
    for (i,Qc) in enumerate(cargas)
        if Qc == carga
            push!(positivos, i)
        elseif Qc == -carga
            push!(negativos, i)
        end
    end
    positivos, negativos
end

# Calcula la distancia menor entre todos los elementos
# de un cluster considerando condiciones periodicas.
function distancia_menor(coord1::Vector{Tuple{T,T}}, coord2::Vector{Tuple{T,T}},
                         renglones::T, columnas::T) where T<:Integer

    dist_cuadrada = renglones^2 + columnas^2

    for r in coord1, s in coord2

        rs1 = abs(r[1]-s[1])
        rs2 = abs(r[2]-s[2])

        if rs1 > renglones/2
            rs1 = renglones - rs1
        elseif rs2 > columnas/2
            rs2 = columnas - rs2
        end

        nueva = rs1^2 + rs2^2
        if nueva < dist_cuadrada
            dist_cuadrada = nueva
        end
    end
    #distancia = sqrt(dist_cuadrada)
    distancia = dist_cuadrada
end

# Propone un apareamiento aleatorio válido para simulated annealing meron-antimeron.
function apareamiento_aleatorio_ma(matriz_distancias::AbstractMatrix{<:Real})
    renglones, columnas = size(matriz_distancias)
    disponibles = collect(1:columnas)
    propuesta = zeros(Int64, renglones)

    for r in 1:renglones
        randi = rand(eachindex(disponibles))
        propuesta[r] = disponibles[randi]
        deleteat!(disponibles, randi)
    end
    propuesta
end

# Propone un apareamiento aleatorio válido para simulated annealing meron-meron.
function apareamiento_aleatorio_mm(n_disp::Integer, n_enlazados::Integer)
    disponibles = collect(1:n_disp)
    propuesta = zeros(Int64, n_enlazados)

    for i in 1:n_enlazados
        randi = rand(eachindex(disponibles))
        propuesta[i] = disponibles[randi]
        deleteat!(disponibles, randi)
    end
    propuesta
end
