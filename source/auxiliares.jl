# Lee una lista de espines y los regresa como una matriz de espines estáticos en julia.
function lector(nombre_datos::String, renglones, columnas)

    datos = readdlm(nombre_datos)
    vol = renglones*columnas
    vol == size(datos,1) || error("Los datos no pueden acomodarse a las dimensiones especificadas.")
    matriz = MMatrix{renglones,columnas,SVector{3,Float64}}()
    for i in eachindex(matriz)
        matriz[i] = SVector(datos[i], datos[i+vol], datos[i+2vol])
    end
    return matriz
end

# Verifica que cada espín en la matriz esté normalizado.
function verifica_norma(matriz::AbstractMatrix{T}, tolerancia = 1e-5
                        ) where T<:AbstractVector{<:Real}
    renglones, columnas = size(matriz)
    for c in 1:columnas, r in 1:renglones
        s1,s2,s3 = matriz[r,c]
        norma = sqrt(s1^2 + s2^2 + s3^2)
        if abs(norma - 1.0) > tolerancia
            error("El espín del renglón [$r,$c] tiene norma = $norma")
        end
    end
    return println("Todos los espines están normalizados.")
end

# Determina el promedio, la desviación estándar y el error
# estándar sobre columnas o renglones de una matriz.
function estadista(M::AbstractMatrix{<:Real}, direccion)

    renglones, columnas = size(M)

    if direccion == :renglones
        promedios = zeros(renglones)
        desviaciones = zeros(renglones)
        errores = zeros(renglones)
        raiz = sqrt(columnas)

        for i in 1:renglones

            valores = M[i,:]
            m = mean(valores)
            s = stdm(valores, m)
            e = s/raiz

            promedios[i] = m
            desviaciones[i] = s
            errores[i] = e
        end
    elseif direccion == :columnas
        promedios = zeros(columnas)
        desviaciones = zeros(columnas)
        errores = zeros(columnas)
        raiz = sqrt(renglones)

        for i in 1:columnas

            valores = M[:,i]
            m = mean(valores)
            s = stdm(valores, m)
            e = s/raiz

            promedios[i] = m
            desviaciones[i] = s
            errores[i] = e
        end
    else
        error("direccion = {:renglones, :columnas}")
    end
    return promedios, desviaciones, errores
end

# Reporta errores con notación sencilla, e.g. π = 3.14159(1).
function reporta_error(x, δx :: AbstractFloat)
    @assert δx < 1 "El error debe ser menor a 1."
    @assert δx < abs(x)

    Δx = string(δx)
    if Δx[end-2] == 'e' && Δx[end-1] == '-'
        decimales = parse(Int, Δx[end])
        cifra_significativa = parse(Int, Δx[1])
    else
        dec = false
        for char in Δx
            if dec
                decimales += 1
                if char != '0'
                    cifra_significativa = parse(Int, char)
                    break
                end
            else
                if char == '.'
                    dec = true
                    decimales = 0
                end
            end
        end
    end

    X = string(x)
    if X[end-2] == 'e' && X[end-1] == '-'
        exp = parse(Int, X[end])
        dif = decimales-exp
        if dif == 0
            truncated_string = string(  X[1],
                                        "(", cifra_significativa, ")",
                                        X[end-2:end] )
        else
            truncated_string = string(  X[1:2],
                                        repeat("0", decimales-exp),
                                        "(", cifra_significativa, ")",
                                        X[end-2:end] )
        end

    else
        no_decimales = 0
        for Char in X
            no_decimales += 1
            if Char == '.'
                break
            end
        end
        dif = decimales + 2 - length(X)
        if dif < 0
            truncated_string = string(  X[1:no_decimales+decimales],
                                        "(", cifra_significativa, ")" )
        else
            truncated_string = string(  X,
                                        repeat("0", dif),
                                        "(", cifra_significativa, ")" )
        end
    end
    return truncated_string
end

# Dado un valor α, busca los índices i y i+1 en
# un vector v creciente tales que v[i] < α < v[i+1].
# También encuentra los índices del rango de certeza considerando los errores:
# El índice mayor tal que (accion+error)[n] < α
# El índice menor tal que (accion-error)[n] > α
function sabueso(acciones, errores, α)
    @assert minimum(acciones) < α < maximum(acciones) "La función nunca toma este valor."

    buscar_cota_menor = true
    buscar_cota_mayor = true
    buscar_media = true

    inf = 0
    sup = 0
    med = 0

    for i in eachindex(acciones)

        if buscar_media && acciones[i] > α
            if abs(acciones[i]-α) < abs(α-acciones[i-1])
                med = i
            else
                med = i-1
            end
            buscar_media = false
        end
        if buscar_cota_menor && acciones[i] + errores[i] > α
            inf = i-1
            buscar_cota_menor = false
        end
        if buscar_cota_mayor && acciones[i] - errores[i] > α
            sup = i
            buscar_cota_mayor = false
        end
    end
    inf, med, sup
end

# Genera el arreglo t⟨S⟩ dado el arreglo ⟨S⟩.
function t_accion(promedios)
    largo = length(promedios)
    cuadrado = zeros(largo)
    for (i,S) in enumerate(promedios)
        cuadrado[i] = i * S
    end
    return cuadrado
end
# Versión con escala temporal real
function t_accion(promedios, errores, h)
    largo = length(promedios)
    tiempos = zeros(largo)
    nueva_escala = zeros(largo)
    nuevo_error = zeros(largo)
    for i in eachindex(promedios)
        t = i*h
        tiempos[i] = t
        nueva_escala[i] = t * promedios[i]
        nuevo_error[i] = t * errores[i]
    end
    return tiempos, nueva_escala, nuevo_error
end

# Toma un vector donde la primera entrada es la suma de n valores y cuya segunda
# entrada es la suma de los cuadrados de dichos valores, donde la tercera
# entrada es el número n de valores sumados, y regresa un vector donde la primera
# entrada es la media correspondiente y la seguna entrada el error estádandar asociado.
function medias_errores(vec::Vector)
    tam = length(vec)
    tam == 3 || error("La matriz debe tener dos renglones.")

    suma, suma2, n = vec
    media = suma ./ n
    desv = suma2 ./ n .- media.^2
    error = sqrt.(desv ./ n)
    return [media, error]
end
# Lo mismo que el anterior pero especificando a priori el número de valores n.
function medias_errores(vec::Vector, n::Integer) # Podría ser in-place
    tam = length(vec)
    tam == 2 || error("El vector debe tener 2 entradas: [suma de valores, suma de sus cuadrados].")

    suma, suma2 = vec
    media = suma ./ n
    desv = suma2 ./ n .- media.^2
    error = sqrt.(desv ./ n)
    return [media, error]
end

# Toma la matriz donde cada entrada del primer renglón es la suma de n valores y cuyo
# segundo renglón es la suma de los cuadrados de dichos valores y regresa una matriz de la
# misma forma donde el primer renglón tiene las medias de cada columna
# y el segundo los errores estándar.
function medias_errores_renglones!(mat::AbstractMatrix{<:Real}, n::Integer)
    ren, col = size(mat)
    ren == 2 || error("La matriz debe tener dos renglones.")
    for c in 1:col
        suma = mat[1,c]
        suma2 = mat[2,c]
        media = suma / n
        desv = suma2 / n - media^2
        error = sqrt(desv / n)
        mat[1,c] = media
        mat[2,c] = error
    end
end
# Hace casi lo mismo que la función anterior pero ahora sólo hay dos columnas, la primera contiene
# la suma de los n valores y la segunda la suma de sus cuadrados. Se agrega un
# parametro extra `vol` que representa el volumen de la configuración para aplicarlo
# directamente al calculo de la susceptibilidad topológica χ = ⟨Q^2⟩ / vol.
function medias_errores_columnas!(mat::AbstractMatrix{<:Real}, n::Integer, vol::Real)
    ren, col = size(mat)
    col == 2 || error("La matriz debe tener dos columnas.")
    for r in 1:ren
        suma = mat[r,1]
        suma2 = mat[r,2]
        media = suma / n
        desv = suma2 / n - media^2
        error = sqrt(desv / n)
        mat[r,1] = media / vol
        mat[r,2] = error / vol
    end
end

# Calcula la carga y su cuadrado de la configuración dada y suma estos valores
# a la matriz de registro en el índice correspondiente al flowstep.
function registra_carga!(registro, σ, flowstep)
    Q = round(abs(triangulador(σ)))
    Q2 = Q^2
    Q4 = Q^4
    indice = flowstep+1
    registro[indice,1] += Q2
    registro[indice,2] += Q4
end
