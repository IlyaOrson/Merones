################################################################################
cd("/home/ilyaorson/Documents/Servicio/Código/Meron/resultados/gfcorr/long_t/adaptativo_M1E5")

using JLD2, Glob, PyPlot

function errores_escalantes(χ, χ_err, ξ, ξ_err)
    χξ2 = χ*ξ^2
    χξ2_err = χξ2 * sqrt((χ_err/χ)^2 + 4*(ξ_err/ξ)^2)
    return χξ2, χξ2_err
end

rango_L = [24,36,54,80, 120,180,270,404] #336
rango_β = [1.263, 1.37, 1.458, 1.535, 1.607, 1.677, 1.743, 1.807] #1.777
for i in eachindex(rango_L)
    L = rango_L[i]
    β = rango_β[i]
    jldopen(glob("*R$L*B$β*.jld2")...) do arch
        susc = arch["susceptibilidad"]
        writedlm("L$L\_B$β\_susc.txt", string.(susc))

        dict = arch["correlacion/sencilla/ajuste"]
        llaves = collect(keys(dict))
        mat = zeros(length(llaves),2)
        for k in sort(llaves)
            tup = dict[k]
            mat[k+1,1] = tup[1]
            mat[k+1,2] = tup[2]
        end
        writedlm("L$L\_B$β\_corr.txt", string.(mat))

        escaling = copy(mat)
        for r in 1:size(mat,1)
            ξ, ξ_err = mat[r,1], mat[r,2]
            χ, χ_err = susc[r,1], susc[r,2]
            χξ2, χξ2_err = errores_escalantes(χ, χ_err, ξ, ξ_err)
            escaling[r,1] = χξ2
            escaling[r,2] = χξ2_err
        end
        writedlm("L$L\_B$β\_escaling.txt", string.(escaling))
    end
end

function compara_hector(rango_L, quant; rango_beta_hector = rango_β)

    for (i,L) in enumerate(rango_L)
        β = rango_beta_hector[i]

        ily_xi_mat = readdlm("L$L\_corr.txt")
        ily_xi, ily_xi_err = ily_xi_mat[:,1], ily_xi_mat[:,2]

        ily_xi2chi_mat = readdlm("L$L\_escaling.txt")
        ily_xi2chi, ily_xi2chi_err = ily_xi2chi_mat[:,1], ily_xi2chi_mat[:,2]

        hec = readdlm("xi2chi_L$(L)beta$β\_t0xi2.txt")
        hec_xi, hec_xi2chi, hec_xi_err, hec_xi2chi_err = hec[:,2],hec[:,3],hec[:,4], hec[:,5]

        flowsteps = 0:length(hec_xi)-1

        if quant == :xi
            errorbar(flowsteps, hec_xi, yerr = hec_xi_err, label = "hector_$L",
                        marker = ".",  linestyle = "--", linewidth = 0.8,
                        markersize = 3, capsize = 4, elinewidth = 1)
            errorbar(flowsteps, ily_xi, yerr = ily_xi_err, label = "ilya_$L",
                        marker = ".",  linestyle = "--", linewidth = 0.8,
                        markersize = 3, capsize = 4, elinewidth = 1)
            ylabel("ξ")

        elseif quant == :xi2chi
            errorbar(flowsteps, hec_xi2chi, yerr = hec_xi2chi_err, label = "hector_$L",
                        marker = ".",  linestyle = "--", linewidth = 0.8,
                        markersize = 3, capsize = 4, elinewidth = 1)
            errorbar(flowsteps, ily_xi2chi, yerr = ily_xi2chi_err, label = "ilya_$L",
                        marker = ".",  linestyle = "--", linewidth = 0.8,
                        markersize = 3, capsize = 4, elinewidth = 1)
            ylabel("χξ²")
        else
            error("quant = {:xi, xi2chi}")
        end
        xlabel("t/t0")
        legend()
    end
end

compara_hector(rango_L[1], :xi, rango_beta_hector = [1.263])
compara_hector(rango_L[2], :xi, rango_beta_hector = ["1.370"])
compara_hector(rango_L[3], :xi, rango_beta_hector = [1.458])
compara_hector(rango_L[4], :xi, rango_beta_hector = [1.535])
compara_hector(rango_L[5], :xi, rango_beta_hector = [1.607])
compara_hector(rango_L[6], :xi, rango_beta_hector = [1.677])

compara_hector(rango_L[1], :xi2chi, rango_beta_hector = [1.263])
compara_hector(rango_L[2], :xi2chi, rango_beta_hector = ["1.370"])
compara_hector(rango_L[3], :xi2chi, rango_beta_hector = [1.458])
compara_hector(rango_L[4], :xi2chi, rango_beta_hector = [1.535])
compara_hector(rango_L[5], :xi2chi, rango_beta_hector = [1.607])


###########################################################################################
####################### Gráficas para el artículo de la conferencia #######################
###########################################################################################

cd("/home/ilyaorson/Documents/Servicio/Código/Meron/resultados/gfcorr/long_t/adaptativo_M1E5")

using Glob, LaTeXStrings
import Meron
reload("Meron")

rango_L = [24,36,54,80,120,180,270,404] # 336
rango_β = [1.263, 1.37, 1.458, 1.535, 1.607, 1.677, 1.743, 1.807] # 1.777

function errores_escalantes(χ, χ_err, ξ, ξ_err)
    χξ2 = χ*ξ^2
    χξ2_err = χξ2 * sqrt((χ_err/χ)^2 + 4*(ξ_err/ξ)^2)
    return χξ2, χξ2_err
end

function hipcorr(L, β)
    Meron.jldopen(glob("*$L*B$β*.jld2")...) do file

        cutoff = cld(L, 3)
        ξ_hint = L/6
        confianza = 0.70

        dict = file["correlacion/sencilla/registro"]
        steps = collect(keys(dict))
        for t in sort(steps)
            mat = dict[t]
            promedios, errores = mat[:,1], mat[:,2]
            Meron.grafCorr(mat[:,1], mat[:,2], cutoff, β, confianza, t)
        end
    end
end
hipcorr(404, 1.807)

function descenso_xe2(rango_L, rango_β, steps) # steps = flowsteps + 1 (includes zero)
    times = 1:steps
    tams = length(rango_L)

    correlaciones = zeros(tams, steps)
    correlaciones_err = zeros(tams, steps)

    escalantes = zeros(tams, steps)
    escalantes_err = zeros(tams, steps)

    for (i, L) in enumerate(rango_L)
        β = rango_β[i]
        mat_susc = readdlm("L$L\_B$β\_susc.txt")
        mat_corr = readdlm("L$L\_B$β\_corr.txt")

        for t in times

            χ = mat_susc[t,1]
            χ_err = mat_susc[t,2]

            ξ = mat_corr[t,1]
            ξ_err = mat_corr[t,2]

            correlaciones[i,t] = ξ
            correlaciones_err[i,t] = ξ_err

            χξ2, χξ2_err = errores_escalantes(χ, χ_err, ξ, ξ_err)
            escalantes[i,t] = χξ2
            escalantes_err[i,t] = χξ2_err
        end
    end
    escalantes, escalantes_err, correlaciones, correlaciones_err
end

function graf_descenso(escalantes, escalantes_err, rango_L, rango_β)
    tams, steps = size(escalantes)
    for i in length(rango_L):-1:1
        L = rango_L[i]
        β = rango_β[i]
        Meron.errorbar( collect(0:steps-1), escalantes[i,:], escalantes_err[i,:],
                        label = "$L   $β", marker = ".",  linestyle = "--", linewidth = 0.8,
                        markersize = 3, capsize = 4, elinewidth = 1)
        # Meron.plot(escalantes[i,:], label = "$L $β")
    end
    # Meron.title(L"Evolution of $\chi \xi ^ 2$")
    Meron.xlabel(L"t/t_0")
    Meron.ylabel(L"\chi_t \xi ^ 2")
    Meron.legend(title = "   L    β", prop = Dict("size"=>10))
    Meron.grid(true)
    # Meron.yscale("log")
end

import PyCall
PyCall.@pyimport matplotlib.ticker as ticker

function graf_ascenso(escalantes, escalantes_err, correlaciones, correlaciones_err)
    tams, steps = size(escalantes)
    fig, ax = Meron.subplots()
    for t in 1:steps
        ebar = ax[:errorbar]( correlaciones[:,t], escalantes[:,t],
                            xerr = correlaciones_err[:,t],
                            yerr = escalantes_err[:,t],
                            label = t-1, marker = ".",
                            markersize = 1, capsize = 2, elinewidth = 1)
        colorin = ebar[1][:get_color]()
    end
    # Meron.title(L"Scaling of $\chi \xi ^ 2$")
    Meron.xlabel(L"\xi", fontsize=15)
    Meron.ylabel(L"\chi_t\, \xi ^ 2", fontsize=15)
    leg = Meron.legend(title = L"t/t_0", prop = Dict("size"=>9))
    Meron.setp(leg[:get_title](), fontsize = 14)
    # Meron.grid(true)
    ax[:set_xscale]("log")
    ax[:set_yscale]("log")

    ax[:set_xticks]([5, 10, 30])
    for tick in ax[:get_xaxis]()[:get_major_ticks]()
        tick[:label][:set_fontsize](14)
    end
    ax[:get_xaxis]()[:set_major_formatter](ticker.ScalarFormatter())

    ax[:set_yticks]([0.1, 0.5, 1])
    for tick in ax[:get_yaxis]()[:get_major_ticks]()
        tick[:label][:set_fontsize](14)
    end
    ax[:get_yaxis]()[:set_major_formatter](ticker.ScalarFormatter())
end

escalantes, escalantes_err, correlaciones, correlaciones_err = descenso_xe2(rango_L, rango_β, 11)
graf_descenso(escalantes, escalantes_err, rango_L, rango_β)
graf_ascenso(escalantes, escalantes_err, correlaciones, correlaciones_err)

######### Ajustes de Wolfgang #########

tams, steps = size(escalantes)
substeps = [1,3,5,7]
params = Dict(  1 => (1.67716, 0.0147174, 1.018),
                3 => (0.308096, 0.0758771, 1.06144),
                5 => (0.197888, 0.109519, 1.06193),
                7 => (0.164081, 0.119327, 1.0601))
fit_func(ξ,c_1,c_2,c_3) = c_1*log(c_2*ξ + c_3)
fit_func(c_1,c_2,c_3) = fit_func(ξ,c_1,c_2,c_3)

import PyCall
PyCall.@pyimport matplotlib.ticker as ticker

fig, ax = Meron.subplots()
for t in substeps
    ebar = ax[:errorbar](  correlaciones[:,t], escalantes[:,t],
                            xerr = correlaciones_err[:,t],
                            yerr = escalantes_err[:,t],
                            label = t-1, marker = ".",
                            linestyle = "none",
                            markersize = 1, capsize = 1.5, elinewidth = 1)
    colorin = ebar[1][:get_color]()
    # @show colorin
    # @show typeof(colorin)

    c1, c2, c3 = params[t]
    fitter(ξ) = fit_func(ξ,c1,c2,c3)
    fit_values = fitter.(correlaciones[:,t])
    ax[:plot](correlaciones[:,t], fit_values, color = colorin)#, linestyle = ":")
end
# Meron.title(L"Scaling of $\chi \xi ^ 2$")
Meron.xlabel(L"\xi", fontsize=19)
Meron.ylabel(L"\chi_t\, \xi ^ 2", fontsize=19)
leg = Meron.legend(title = L"t/t_0", prop = Dict("size"=>12))
Meron.setp(leg[:get_title](), fontsize = 14)
# Meron.grid(true)

ax[:set_xscale]("log")
ax[:set_yscale]("linear")

ax[:set_xticks]([5, 10, 50])
for tick in ax[:get_xaxis]()[:get_major_ticks]()
    tick[:label][:set_fontsize](14)
end
ax[:get_xaxis]()[:set_major_formatter](ticker.ScalarFormatter())

ax[:set_yticks]([0.1, 0.5, 1])
for tick in ax[:get_yaxis]()[:get_major_ticks]()
    tick[:label][:set_fontsize](14)
end
ax[:get_yaxis]()[:set_major_formatter](ticker.ScalarFormatter())


Meron.title("fits to \$c_1 ln(c_2 \\xi + c_3)\$", fontsize = 16)
