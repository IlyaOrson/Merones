### Archivo para correr en el cluster.

if length(ARGS) != 7
	error("Necesito 7 argumentos: renglones::Int columnas:Int sweeps_entre_medidas::Int medidas::Int pasos::Int h::Float beta::Float")
else
	renglones 	= parse(Int64, ARGS[1])		# Renglones de la configuración
	columnas 	= parse(Int64, ARGS[2])		# Columnas de la configuración
	sweeps 		= parse(Int64, ARGS[3]) 	# Corridas multicluster entre cada medición
	medidas 	= parse(Int64, ARGS[4]) 	# Número de ocasiones que se ejecutará el algoritmo Gradient Flow
	pasos		= parse(Int64, ARGS[5])		# Pasos temporales del Gradient Flow
	h 			= parse(Float64, ARGS[6])	# Tamaño del paso temporal
	β 			= parse(Float64, ARGS[7])	# β de la acción estándar
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
push!(LOAD_PATH, "/home/ilya/Documents/Servicio/Código/Meron/source")

using Meron

σ = [Meron.SVector(1.0,0.0,0.0) for i in 1:renglones, j in 1:columnas]

tf = pasos * h
to = 0

@time datos = Meron.sporadicGF(σ, sweeps, medidas, β, to, tf, h)

prom, stad, erro = Meron.estadista(datos, :renglones)

open("promedios_accion_GF_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_P$pasos\_H$h\_B$β\.txt", "w") do file
	writedlm(file, hcat(prom,erro))
end
