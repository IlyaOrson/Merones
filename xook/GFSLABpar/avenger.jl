#!/bin/bash

if length(ARGS) != 10
	error("\n\nNecesito 10 argumentos:\n\nrenglones::Int\ncolumnas:Int\nsweeps_entre_medidas::Int\nmedidas::Int\nqmax::Int\nflow_time::Float\nflow_steps::Int\nh::Float\nbeta::Float\njobs::Int\n")
else
	renglones 	= parse(Int64, ARGS[1])		# Renglones de la configuración
	columnas 	= parse(Int64, ARGS[2])		# Columnas de la configuración
	sweeps 		= parse(Int64, ARGS[3]) 	# Corridas multicluster entre cada medición
	medidas 	= parse(Int64, ARGS[4]) 	# Número de ocasiones que se ejecutará el algoritmo Gradient Flow
    Qmax        = parse(Int64, ARGS[5])     # Registrar medidas para Q = [0,1,...,Qmax]
	t0          = parse(Float64, ARGS[6])	# Flow time t0 tal que t0⟨s⟩ = 0.1
	p 			= parse(Int64, ARGS[7])		# Flow steps: [0,t0,2t0,...,pt0]
	h 			= parse(Float64, ARGS[8])	# Tamaño del paso temporal
	β 			= parse(Float64, ARGS[9])	# β de la acción estándar
	j 			= parse(Int64, ARGS[10])	# Número de jobs en la corrida
end

using Glob

cd("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_Q$Qmax\_T$t0\_P$p\_H$h\_B$β\_J$j")

corridas = glob("*J*")
cuantas_corridas = length(corridas)
medidas_totales = medidas*cuantas_corridas

mkdir("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_Q$Qmax\_T$t0\_P$p\_H$h\_B$β")
cd("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_Q$Qmax\_T$t0\_P$p\_H$h\_B$β")

tiempos = [n*t0 for n in 0:p]

################ Susceptibilidad topológica medida directamente ################

archivos_χ = glob("../*/topo*")
cuantos_archivos_χ = length(archivos_χ)

resultados_χ = zeros(p+1,3)
resultados_χ[:,1] = tiempos

for name in archivos_χ
	mat = readdlm(name)
	for P in 1:p+1
		resultados_χ[P,2] += mat[P,2]		# medias
		resultados_χ[P,3] += mat[P,3]^2		# errores
	end
end

for P in 1:p+1
	resultados_χ[P,2] /= cuantos_archivos_χ
	resultados_χ[P,3] = sqrt(resultados_χ[P,3]) / cuantos_archivos_χ
end

open("topo_chi_GFSLAB_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_QMAX$Qmax\_To$t0\_P$p\_H$h\_B$β.txt", "w") do file
	writedlm(file, resultados_χ)
end

################################# Correlación #################################

for t in tiempos
	archivos_corr = glob("../*/corr*T$t*")
	cuantos_archivos_corr = length(archivos_corr)

	resultados_corr = zeros(renglones+1,2)

	for name in archivos_corr
		mat = readdlm(name)
		for ren in 1:renglones+1
			resultados_corr[ren,1] += mat[ren,1]		# medias
			resultados_corr[ren,2] += mat[ren,2]^2		# errores
		end
	end

	for ren in 1:renglones+1
		resultados_corr[ren,1] /= cuantos_archivos_corr
		resultados_corr[ren,2] = sqrt(resultados_corr[ren,2]) / cuantos_archivos_corr
	end

	open("corr_GFSLAB_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_QMAX$Qmax\_T$t\_P$p\_H$h\_B$β.txt", "w") do file
		writedlm(file, resultados_corr)
	end
end

#################################### Slabs ####################################

for t in tiempos
	for Q in 0.0:Qmax

		archivos_slab = glob("../*/prom-err*Q$Q*T$t*")
		cuantos_archivos_slab = length(archivos_slab)

		if cuantos_archivos_slab == 0	# no siempre hay datos para todas las cargas
			continue
		end

		resultados_slab = zeros(renglones,2)

		for name in archivos_slab
			mat = readdlm(name)
			for ren in 1:renglones
				resultados_slab[ren,1] += mat[ren,1]		# medias
				resultados_slab[ren,2] += mat[ren,2]^2		# errores
			end
		end

		for ren in 1:renglones
			resultados_slab[ren,1] /= cuantos_archivos_slab
			resultados_slab[ren,2] = sqrt(resultados_slab[ren,2]) / cuantos_archivos_slab
		end

		open("prom-err_GFSLAB_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_Q$Q\_T$t\_P$p\_H$h\_B$β.txt", "w") do file
			writedlm(file, resultados_slab)
		end
	end
end
