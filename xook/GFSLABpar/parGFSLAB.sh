#!/bin/bash

if [[ $# -ne 10 ]]
then
    echo "
    Necesito 10 argumentos (separados por un espacio):

    renglones::Int
    columnas:Int
    sweeps_entre_medidas::Int
    medidas::Int
    qmax::Int
    flow_time::Float
    flow_steps::Int
    h::Float
    beta::Float
    corridas_paralelas::Int64
    "
else

    mkdir res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9\_J${10}
    cd res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9\_J${10}

    for i in `seq 1 ${10}`;
    do
        mkdir res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9\_J$i
        cd res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9\_J$i

        cp ../../*.pbs .

        sed -i "s/RENGLONES/$1/g"  *.pbs
        sed -i "s/COLUMNAS/$2/g"   *.pbs
        sed -i "s/SWEEPS/$3/g"     *.pbs
        sed -i "s/MEDIDAS/$4/g"    *.pbs
        sed -i "s/QMAX/$5/g"       *.pbs
        sed -i "s/FLOWTIME/$6/g"   *.pbs
        sed -i "s/FLOWSTEPS/$7/g"  *.pbs
        sed -i "s/HP/$8/g"         *.pbs
        sed -i "s/BETA/$9/g"       *.pbs
        sed -i "s/JOB/$i/g"       *.pbs

        qsub *.pbs

        cd ..
        sleep 0.4
    done
fi
