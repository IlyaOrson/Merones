if length(ARGS) != 4
	error("\n\nNecesito 4 argumentos:\n\nlado:Int\nbeta::Float\nflowsteps::Int\njob::Int\n\n")
else
	L 			= parse(Int64, ARGS[1])		# Lado de la configuración
	β 			= parse(Float64, ARGS[2])	# β de la acción estándar
	flowsteps   = parse(Int64, ARGS[3])		# flowsteps por calcular
	job 		= parse(Int64, ARGS[4])		# etiqueta para uso en paralelo
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
push!(LOAD_PATH, "/home/ilya/Documents/Servicio/Código/Meron/source")

import Meron

termalizacion = 10^6
Δts_rk4 = [0.001, 0.0001]
Δts_rk45 = [0.0001]
tols = [1E-6, 1E-7]

nombre = "multitemporal_L$L\_B$β\_FS$flowsteps\_J$job.jld2"

@time Meron.generador_config(nombre, L, β, termalizacion)
@time Meron.recorder(nombre, flowsteps, Δts_rk4)
@time Meron.recorder(nombre, flowsteps, Δts_rk45, tols)
