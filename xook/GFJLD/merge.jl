if length(ARGS) != 1
	error("\n\nNecesito el nombre de la carpeta con resultados:\n\ncarpeta::String\n\n")
else
	carpeta = ARGS[1]
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
push!(LOAD_PATH, "/home/ilya/Documents/Servicio/Código/Meron/source")

using Glob, JLD2
import Meron

# revisar que el análisis no se haya realizado antes
resultados_previos = glob("resultados*.jld2")
if length(resultados_previos) != 0
	error("ya existen archivos con resultados: $resultados_previos")
end

cd(carpeta)
archivos = glob("*.jld2")
jobs = length(archivos)
if jobs == 0; error("¡no hay archivos jld2!"); end

const_keys = ["RK4/Δt:0.001", "RK4/Δt:0.0001"]
adap_keys = ["RK45/Δt:0.0001/tol:1.0e-6", "RK45/Δt:0.0001/tol:1.0e-7"]
all_keys = vcat(const_keys, adap_keys)
ref_key = "RK4/Δt:0.0001"
other_keys = ["RK4/Δt:0.001", "RK45/Δt:0.0001/tol:1.0e-6", "RK45/Δt:0.0001/tol:1.0e-7"]

# determinar parámetros a partir del primer archivo
jldopen(archivos[1]) do primer

	param = primer["parametros"]

	global L = param["L"]
	global β = param["β"]
	global flowtime = param["flowtime"]
	global terma_sweeps = param["terma_sweeps"]

	# las tolerancias no dependen de Δt inicial
	global Δts = keys(primer["RK45"])
	global tols = keys(primer["RK45"][Δts[1]])
end

function distance_at_flowsteps(results_file, ref_key)
	reference_confs = results_file[ref_key]["σ"]
	comparison = Dict()
	for k in other_keys
		dif = map(Meron.phil_error, results_file[k]["σ"], reference_confs)
		comparison[k * " - " * ref_key] = dif
	end
	return comparison
end

function Q2_at_flowsteps(results_file, all_keys)
	results_file["Q"]
	comparison = Dict()
	for k in all_keys
		all_Q = vcat(results_file["Q"], results_file[k]["Q"])
		all_Q2 = map(x -> x^2, all_Q)
		comparison[k] = all_Q2
	end
	return comparison
end

function timestep_at_flowsteps(results_file, adap_keys)
	comparison = Dict()
	for keys in adap_keys
		all_Δts = vcat(
			results_file[keys]["Δt_inicial"],
			results_file[keys]["Δt_final"]
			)
		comparison[keys] = all_Δts
	end
	return comparison
end

function integrator_cronometer(results_file, ref_key)
	comparison = Dict()
	ref_time = sum(results_file[ref_key]["cronometro"])
	for	k in other_keys
		time_taken = sum(results_file[k]["cronometro"])
		comparison[k * " - " * ref_key] = time_taken / ref_time
	end
	return comparison
end

# ir sumando valores en diccionario
function summer!(dict, key, values) # value::Array
	current = get!(dict, key, zeros(length(values),2))
	current[:,1] .+= values
	current[:,2] .+= values.^2
	return nothing
end
function summer!(main_dict, sample_dict)
	for k in keys(sample_dict)
		summer!(main_dict, k, sample_dict[k])
	end
end

function errors!(mat::AbstractArray{T,N} where {T,N}, jobs)
	mean = mat[:,1] ./ jobs
	square_mean = mat[:,2] ./ jobs
	std_error = sqrt.((square_mean .- (mean.^2)) ./ jobs)
	mat[:,1] = mean
	mat[:,2] = std_error
end
function errors!(results_dict::Associative{T,N} where {T,N}, jobs)
	for (key, value) in results_dict
		errors!(value, jobs)
	end
end

distances 		= Dict()
timesteps		= Dict()
charges 		= Dict()
cronometers		= Dict()

for nombre in archivos
	jldopen(nombre) do results_file

		sample_distances = distance_at_flowsteps(results_file, ref_key)
		sample_charges = Q2_at_flowsteps(results_file, all_keys)
		sample_timesteps = timestep_at_flowsteps(results_file, adap_keys)
		sample_cronometers = integrator_cronometer(results_file, ref_key)

		summer!(distances, sample_distances)
		summer!(timesteps, sample_timesteps)
		summer!(charges, sample_charges)
		summer!(cronometers, sample_cronometers)
	end
end

errors!(distances, jobs)
errors!(timesteps, jobs)
errors!(charges, jobs)
errors!(cronometers, jobs)

# salvar todos los diccionarios con resultados en un único archivo
nombre_resultados = "resultados_L$L\_FT$flowtime\_B$β\_JT$jobs.jld2"
jldopen(nombre_resultados, "w") do archivo

	archivo["L"] 			= L
	archivo["β"] 			= β
	archivo["flowtime"] 	= flowtime
	archivo["terma_sweeps"] = terma_sweeps

	archivo["distances"] 	= distances
	archivo["timesteps"] 	= timesteps
	archivo["charges"] 		= charges
	archivo["cronometers"] 	= cronometers
end
