#!/bin/bash

if [[ $# -ne 4 ]]
then
    echo "
    Necesito 4 argumentos (separados por un espacio):

    tamaño::Int
    beta::Float
    flowsteps::Int
    corridas_paralelas::Int
    "
else
    mkdir GFJLD_L$1\_B$2\_FS$3\_JT$4
    cd GFJLD_L$1\_B$2\_FS$3\_JT$4

    for i in `seq 1 $4`;
    do
        cp ../*.pbs .

        sed -i "s/LADO/$1/g"        *.pbs
        sed -i "s/BETA/$2/g"        *.pbs
        sed -i "s/FLOWSTEPS/$3/g"   *.pbs
        sed -i "s/JOB/$i/g"         *.pbs

        qsub *.pbs
        # cat *.pbs
        rm *.pbs
        sleep 0.3
    done
fi
