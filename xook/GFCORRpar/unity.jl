#!/bin/bash

if length(ARGS) != 10
	error("\n\nNecesito 10 argumentos:\n\nrenglones::Int\ncolumnas:Int\nsweeps_entre_medidas::Int\nmedidas::Int\nflow_time::Float\nflow_steps::Int\nh::Float\nbeta::Float\nadapt::Bool\njobs::Int\n")
else
	renglones 	= parse(Int64, ARGS[1])		# Renglones de la configuración
	columnas 	= parse(Int64, ARGS[2])		# Columnas de la configuración
	sweeps 		= parse(Int64, ARGS[3]) 	# Corridas multicluster entre cada medición
	medidas 	= parse(Int64, ARGS[4]) 	# Número de ocasiones que se ejecutará el algoritmo Gradient Flow
	t0          = parse(Float64, ARGS[5])	# Flow time t0 tal que t0⟨s⟩ = 0.1
	p 			= parse(Int64, ARGS[6])		# Flow steps: [0,t0,2t0,...,pt0]
	h 			= parse(Float64, ARGS[7])	# Tamaño del paso temporal
	β 			= parse(Float64, ARGS[8])	# β de la acción estándar
	adapt 		= parse(Bool, ARGS[9])		# utilizar RK de 4to orden... ¿adaptativo?
	j 			= parse(Int64, ARGS[10])	# Número de jobs en la corrida
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
using Glob, JLD2
import Meron

cd("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_T$t0\_P$p\_H$h\_B$β\_A$adapt\_J$j")

corridas = glob("*J*")
cuantas_corridas = length(corridas)
medidas_totales = medidas*cuantas_corridas

mkdir("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_T$t0\_P$p\_H$h\_B$β\_A$adapt")
cd("res_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_T$t0\_P$p\_H$h\_B$β\_A$adapt")

nombre_archivo = "resultados_R$renglones\_C$columnas\_S$sweeps\_M$medidas_totales\_To$t0\_P$p\_H$h\_B$β\_A$adapt.jld2"

################ Susceptibilidad topológica medida directamente ################

archivos_χ = glob("../*/topo*")
cuantos_archivos_χ = length(archivos_χ)

resultados_χ = zeros(p+1,3)
# resultados_χ[:,1] = tiempos
# No trabajar con la primer columna que en los registros viejos representaba el timpo

for name in archivos_χ
	mat = readdlm(name)
	for P in 1:p+1
		resultados_χ[P,2] += mat[P,2]		# medias
		resultados_χ[P,3] += mat[P,3]^2		# errores
	end
end

for P in 1:p+1
	resultados_χ[P,2] /= cuantos_archivos_χ
	resultados_χ[P,3] = sqrt(resultados_χ[P,3]) / cuantos_archivos_χ
end

################################# Correlación #################################

registro_correlaciones_sencilla = Dict(n => zeros(columnas,2) for n in 0:p)
registro_ajustes = Dict{Int, NTuple{5,Float64}}()
registro_escalante = Dict{Int, NTuple{2,Float64}}()
cutoff = cld(columnas, 3)
confianza = 0.95

for n in 0:p # slow_steps
	t = n*t0
	archivos_corr = glob("../*/corr*T$t*")
	cuantos_archivos_corr = length(archivos_corr)

	resultados_corr = zeros(renglones+1,2)

	for name in archivos_corr
		mat = readdlm(name)
		for ren in 1:renglones+1
			resultados_corr[ren,1] += mat[ren,1]		# medias
			resultados_corr[ren,2] += mat[ren,2]^2		# errores
		end
	end

	for ren in 1:renglones+1
		resultados_corr[ren,1] /= cuantos_archivos_corr
		resultados_corr[ren,2] = sqrt(resultados_corr[ren,2]) / cuantos_archivos_corr
	end
	registro_correlaciones_sencilla[n] = resultados_corr

	promedios = resultados_corr[:,1]
	errores = resultados_corr[:,2]

	ξ, ξ_err, goodness =
	Meron.ajustador_cosh(promedios, errores, cutoff, confianza)
	razon = columnas / ξ
	razon_err = columnas * ξ_err / ξ^2
	registro_ajustes[n] = ξ, ξ_err, goodness, razon, razon_err

	χ = resultados_χ[n+1, 2]
	χ_err = resultados_χ[n+1, 3]

	χξ2 = χ*ξ^2
	χξ2_err = χξ2 * sqrt((χ_err/χ)^2 + 4*(ξ_err/ξ)^2)

	registro_escalante[n] = χξ2, χξ2_err
end

jldopen(nombre_archivo, "w") do archivo
	archivo["susceptibilidad"] = resultados_χ[:,2:3] # No guardar columna con tiempo
	archivo["correlacion/sencilla/registro"] = registro_correlaciones_sencilla
	archivo["correlacion/sencilla/ajuste"] = registro_ajustes
	archivo["escalante/sencilla"] = registro_escalante
end
