#!/bin/bash

if [[ $# -ne 9 ]]
then
    echo "
    Necesito 9 argumentos (separados por un espacio):
    
    renglones::Int
    columnas:Int
    sweeps_entre_medidas::Int
    medidas::Int
    qmax::Int
    flow_time::Float
    flow_steps::Int
    h::Float
    beta::Float
    "
else
    mkdir res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9
    cd res_R$1\_C$2\_S$3\_M$4\_Q$5\_T$6\_P$7\_H$8\_B$9

    cp ../*.pbs .

    sed -i "s/RENGLONES/$1/g"  *.pbs
    sed -i "s/COLUMNAS/$2/g"   *.pbs
    sed -i "s/SWEEPS/$3/g"     *.pbs
    sed -i "s/MEDIDAS/$4/g"    *.pbs
    sed -i "s/QMAX/$5/g"       *.pbs
    sed -i "s/FLOWTIME/$6/g"   *.pbs
    sed -i "s/FLOWSTEPS/$7/g"  *.pbs
    sed -i "s/HP/$8/g"         *.pbs
    sed -i "s/BETA/$9/g"       *.pbs

    qsub *.pbs
fi
