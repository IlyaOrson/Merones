if length(ARGS) != 9
	error("\n\nNecesito 9 argumentos:\n\nrenglones::Int\ncolumnas:Int\nsweeps_entre_medidas::Int\nmedidas::Int\nflow_time::Float\nflow_steps::Int\nh::Float\nbeta::Float\nadapt::Bool")
else
	renglones 	= parse(Int64, ARGS[1])		# Renglones de la configuración
	columnas 	= parse(Int64, ARGS[2])		# Columnas de la configuración
	sweeps 		= parse(Int64, ARGS[3]) 	# Corridas multicluster entre cada medición
	medidas 	= parse(Int64, ARGS[4]) 	# Número de ocasiones que se ejecutará el algoritmo Gradient Flow
	t0          = parse(Float64, ARGS[5])	# Flow time t0 tal que t0⟨s⟩ = 0.1
	p 			= parse(Int64, ARGS[6])		# Flow steps: [0,t0,2t0,...,pt0]
	h 			= parse(Float64, ARGS[7])	# Tamaño del paso temporal
	β 			= parse(Float64, ARGS[8])	# β de la acción estándar
	adapt 		= parse(Bool, ARGS[9])		# utilizar RK de 4to orden... ¿adaptativo?
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
push!(LOAD_PATH, "/home/ilya/Documents/Servicio/Código/Meron/source")

import Meron

σ = [Meron.SVector(1.0,0.0,0.0) for i in 1:renglones, j in 1:columnas]

@time registro_correlaciones, registro_χ = Meron.sliced_correlationGF(σ, sweeps, medidas, t0, p, h, β; adaptive = adapt);

for t in keys(registro_correlaciones)
	mat_corr = registro_correlaciones[t]
	mat_corr = hcat(mat_corr, mat_corr[:,1])
	open("corr_GFCORR_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_T$t\_P$p\_H$h\_B$β\_A$adapt.txt", "w") do file
		writedlm(file, mat_corr.')
	end
end

open("topo_chi_GFCORR_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_To$t0\_P$p\_H$h\_B$β\_A$adapt.txt", "w") do file
	tiempos = [n*t0 for n in 0:p]
	writedlm(file, hcat(tiempos, registro_χ))
end
