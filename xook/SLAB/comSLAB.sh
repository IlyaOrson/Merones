#!/bin/bash

if [[ $# -ne 6 ]]
then
    echo "
    Necesito 6 argumentos:
    renglones::Int
    columnas:Int
    sweeps_entre_medidas::Int
    medidas::Int
    Qmax::Int
    beta::Float
    "
else
    mkdir res_R$1\_C$2\_S$3\_M$4\_Q$5\_B$6
    cd res_R$1\_C$2\_S$3\_M$4\_Q$5\_B$6

    cp ../*.pbs .

    sed -i "s/RENGLONES/$1/g"  *.pbs
    sed -i "s/COLUMNAS/$2/g"   *.pbs
    sed -i "s/SWEEPS/$3/g"     *.pbs
    sed -i "s/MEDIDAS/$4/g"    *.pbs
    sed -i "s/QMAX/$5/g"       *.pbs
    sed -i "s/BETA/$6/g"       *.pbs

    qsub *.pbs
fi
