### Archivo para correr en el cluster.

if length(ARGS) != 6
	error("\n\nNecesito 6 argumentos:\n\nrenglones::Int\ncolumnas:Int\nsweeps_entre_medidas::Int\nmedidas::Int\nQmax::Int\nbeta::Float\n")
else
	renglones 	= parse(Int64, ARGS[1])		# Renglones de la configuración
	columnas 	= parse(Int64, ARGS[2])		# Columnas de la configuración
	sweeps 		= parse(Int64, ARGS[3]) 	# Corridas multicluster entre cada medición
	medidas 	= parse(Int64, ARGS[4]) 	# Número de ocasiones que se ejecutará el algoritmo Slab Method
	Qmax		= parse(Int64, ARGS[5])		# Pasos temporales del Slab Method
	β 			= parse(Float64, ARGS[6])	# β de la acción estándar
end

push!(LOAD_PATH, "/storage/icn/ilyaorson/source")
push!(LOAD_PATH, "/home/ilya/Documents/Servicio/Código/Meron/source")

using Meron

σ = [Meron.SVector(1.0,0.0,0.0) for i in 1:renglones, j in 1:columnas]

@time registros_q2_prima = slabber(σ, Qmax, sweeps, medidas, β)

for Q in keys(registros_q2_prima)
	prom, erro = registros_q2_prima[Q]
	open("prom_err_SLAB_R$renglones\_C$columnas\_S$sweeps\_M$medidas\_Q$Q\_B$β\.txt", "w") do file
		writedlm(file, hcat(prom,erro))
	end
end
