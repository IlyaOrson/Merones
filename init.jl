σ = Meron.rand_spins(80,80; container = :normal);
Σ = copy(σ); wolff = Meron.rand_spin(); β = 2.5;
cstat = copy(σ); cadap = copy(σ);

@time sta_act, sta_err = Meron.gradient_flow_action!(copy(σ), β, 0.0, 1.0, 0.0001);
@time ada_h, ada_S, ada_t_acept, ada_t_all, ada_err = Meron.adaptive_gradient_flow_action!(copy(σ), β, 0.0, 1.0, 0.0001);
